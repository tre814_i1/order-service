# Application multi-services : Order-Service

Auteur :
Mistayan - [Gitlab](https://gitlab.com/Mistayan/)

[TOC]

## Description

Ce projet est un exemple d'application multi-services utilisant Spring Boot et Spring Cloud. Il est composé de 3
services :

- order-service : Service de gestion des commandes
- product-service : Service de gestion des produits
- customer-service : Service de gestion des clients

Ainsi que d'un Broker, permettant la communication entre les services.

- Kafka : Broker de messageries AVANCE

## Technologies

- Java 21
- Spring Boot 3.2.4
- Postgresql
- JPA
- Docker

## Installation

1. Cloner le projet
    ```shell
    git clone https://gitlab.com/tre814_i1/order-service.git 
    ```
2. Se rendre dans le dossier du projet
    ```shell
    cd order-service
    ```

3. Lancer la commande `./graldew bootBuildImage` pour construire le programme en Image:
    ```shell
    ./gradlew bootBuildImage
    ```
4. Créer un fichier .env a vous:

```shell
cp .env.example .env
nano .env
```

5. Puis `docker compose up -d` pour lancer les conteneurs docker

```shell
    docker compose -f compose.globalTest.yml up -d
   ```

5. Attendre que les conteneurs soient prêts, puis lancer la commande `docker compose ps` pour vérifier que les
   conteneurs
   sont bien démarrés
    ```shell
    docker compose -f compose.globalTest.yml ps
    ```

Les services Kafka sont accessibles aux adresses :

- http://localhost:9092 → Kafka Broker (réservé à la communication interne des services)
- http://localhost:29092 → Kafka Admin
- http://localhost:38090 → Kafka UI
- http://localhost:38090 → Kafka Zookeeper

----

6. Pour arrêter les conteneurs, lancer la commande `docker compose down`
    ```shell
    docker compose -f compose.globalTest.yml down
    ```

8. Pour supprimer les images récupérées, lancer la commande :
    ```shell
      docker rmi $(docker compose -f compose.globalTest.yml images -q)
    ```

## Utilisation

### Application globale

<table>
  <tr>
    <th>Service</th>
    <th>Port</th>
    <th>URL</th>
    <th>Lien du projet</th>
  </tr>
  <tr>
    <td>customer-service</td>
    <td>30050</td>
    <td>http://localhost:30050</td>
    <td><a href="https://gitlab.com/tre814_i1/customer-service">Lien</a></td>
  </tr>
 <tr>
    <td>product-service</td>
    <td>30055</td>
    <td>http://localhost:30055</td>
    <td><a href="https://gitlab.com/tre814_i1/product-service">Lien</a></td>
  </tr>
  <tr>
    <td>order-service</td>
    <td>30045</td>
    <td>http://localhost:30045</td>
    <td>Le projet actuel</td>
</tr>
<tr>
    <td>Shared Objects</td>
    <td>-</td>
    <td>-</td>
    <td><a href="https://gitlab.com/tre814_i1/shared_objects">Lien</a></td>
</table>

# API

____________________________________________

____

### Order-service

____

#### Créer une commande

- URL : `http://localhost:30045/orders`
- Méthode : POST
- Body :

```json
{
  "customerId": "11",
  "products": [
    "..."
  ]
}
```

#### Récupérer une commande

- URL : `http://localhost:30045/orders/{id}`
- Méthode : GET
- Paramètre : id
- Exemple : `http://localhost:30045/orders/3`
- Réponse :

```json
{
  "createdAt": "2023-08-30T08:35:27.416Z",
  "id": "3",
  "customerId": "11",
  "products": [
    {
      "createdAt": "2023-08-30T03:16:28.440Z",
      "name": "Rufus Pacocha",
      "details": {
        "price": "97.00",
        "description": "The Football Is Good For Training And Recreational Purposes",
        "color": "red"
      },
      "stock": 73253,
      "id": "3",
      "orderId": "3"
    },
    {
      "createdAt": "2023-08-29T22:55:14.728Z",
      "name": "Paula Hodkiewicz",
      "details": {
        "price": "139.00",
        "description": "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients",
        "color": "salmon"
      },
      "stock": 1609,
      "id": "53",
      "orderId": "3"
    }
  ]
}
```

______
______

# Fonctionnement interne, en incluant le Broker Kafka

Utilisant une BDD par service, il est conseillé d'utiliser le
pattern [SAGA](https://microservices.io/patterns/data/saga.html) :

- https://microservices.io/patterns/data/database-per-service.html

OrderService porte la responsabilité de la transaction, et doit être capable d'annuler la commande si un service n'a pas
pu valider la commande.
Il devra aussi propager les erreurs aux services concernés. (gestion native avec Kafka-Spring: @Transactional(
propagation = Propagation.REQUIRED))
___

### 0/ L'utilisateur crée une nouvelle commande pour le client 1, incluant les articles 1, 2 et 3

Il faut initier une `transaction` par service effectuant un `Process` recevant des instructions, afin de pouvoir revenir
en arrière si l'un des services n'a pas pu valider la commande.
____

### 1/ On crée une `commande temporaire`, pour réserver les articles en stock et l'associer au client.

```mermaid
sequenceDiagram
    OrderService ->> Order DB: Créer une commande temporaire
    Order DB ->> OrderService: Commande enregistrée {id: 1, ...}
```

L'`orderId` nous sert d'`id de transaction`, pour pouvoir annuler la commande si un service n'a pas pu valider la
commande.
____

### 2/ On doit vérifier la disponibilité des `articles en stock`, et renseigner la `commande` chez le `client`.

- à l'avenir, il est aussi possible de réduire le portefeuille client

#### __Enregistrement de la commande du Customer__ :

- Topic : customer-request : `CustomerRequest{orderId, customerId}`
- Topic : customer-response : `CustomerResponse{orderId, Customer{id: 1, ...}}`

```mermaid
sequenceDiagram
    OrderService -->> Broker: CustomerRequest {key, customerId: 1}
    CustomerService ->> Broker: Listen CustomerRequest
    CustomerService ->> CustomerService: Trouver le client et ajouter la commande
    CustomerService -->> Broker: Response
    Broker ->> OrderService: CustomerResponse {key, Customer{id: 1, ...}}
```

____

#### __Réservation des produits__ :

- Topic: stock-request : `StockRequest{key, ProductRequest[]: [{id, qty(Wanted)}, ...], StockOperation}`
- Topic: stock-response : `StockResponse{key, Product[]: [{id, qty(Available), unit_price}, ...]}`

```mermaid
sequenceDiagram
    OrderService ->> Broker: StockRequest {orderId, [{id:1, qty:3} , ...], op:SUB}
    Broker -->> ProductService: StockRequest
    ProductService -> ProductService: Trouver les produits, Décrémenter le stock
    ProductService ->> Broker: StockResponse
    Broker -->> OrderService: StockResponse {orderId, products: [{id:1, qty:512, ...} , ...]}
``` 

______

## Services Compatibility Mapping

| OrderService | ProductService | CustomerService | SharedModels |
|--------------|----------------|-----------------|--------------|
| v0.5.0       | v0.0.6         | v0.0.6          | v0.0.3       |
| v0.6.1       | v0.4.0         | v0.0.7          | v0.0.4       |
| v0.6.3+      | v0.4.3         | v0.0.7+         | v0.0.4       |

## Licence

Ce projet est sous licence MIT - voir le fichier [LICENSE.md](LICENSE.md) pour plus d'informations

## Contacts

Mistayan - [Gitlab](https://gitlab.com/Mistayan/)
Bob-117 - [Gitlab](https://gitlab.com/Bob-117/)
Sullfurick - [Gitlab](https://gitlab.com/Sullfurick)