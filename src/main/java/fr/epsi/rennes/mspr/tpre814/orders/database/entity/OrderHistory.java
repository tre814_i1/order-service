package fr.epsi.rennes.mspr.tpre814.orders.database.entity;

import fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity(name = "OrderHistory")
@Table(name = "order_history",
        uniqueConstraints = {    @UniqueConstraint(columnNames = {"order_id", "status"})}
)
@Getter
//@EntityListeners(OrderHistoryListener.class)
public final class OrderHistory {

    @CreatedDate
    @CreationTimestamp
    @Column(name = "dt", nullable = false, updatable = false)
    private final LocalDateTime at;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Linked to Orders. If order disappears, history is not relevant anymore
    @ManyToOne
    @JoinColumn(name = "order_id", referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_order_history_order"),
            nullable = false, updatable = false
    )
    private @NotNull Order order;
    @Column(name = "status", nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private OrderStatus status;


    public OrderHistory() {
        this.at = LocalDateTime.now();
    }

    public OrderHistory(UUID orderId, OrderStatus status) {
        this();
        Order o = new Order();
        o.setId(orderId);
        this.order = o;
        this.status = status;
    }
}
