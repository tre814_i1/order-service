package fr.epsi.rennes.mspr.tpre814;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Slf4j
@SpringBootApplication
@EnableAsync
@EnableJpaAuditing
@EnableAspectJAutoProxy
//@EnableDiscoveryClient
public class OrderManagerApplication {
  @Bean
  public OncePerRequestFilter logFilter() {
    return new OncePerRequestFilter() {
      @Override
      protected void doFilterInternal( HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        log.info("Path: {}", request.getRequestURI());
        filterChain.doFilter(request, response);
        log.info("Path: {} {}", request.getRequestURI(), response.getStatus());
      }
    };
  }
    public static void main(String[] args) {
        SpringApplication.run(OrderManagerApplication.class, args);
    }
}
