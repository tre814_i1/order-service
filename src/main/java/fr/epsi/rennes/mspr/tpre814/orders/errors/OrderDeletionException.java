package fr.epsi.rennes.mspr.tpre814.orders.errors;

public class OrderDeletionException extends RuntimeException {
    public OrderDeletionException(String s) {
        super(s);
    }
}
