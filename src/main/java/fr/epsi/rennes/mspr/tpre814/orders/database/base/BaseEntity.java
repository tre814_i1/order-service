package fr.epsi.rennes.mspr.tpre814.orders.database.base;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

// Base entity includes an auto-id field for all entities and timestamps created and updated
@MappedSuperclass
@Getter
@Setter
public abstract class BaseEntity implements Serializable {
    private static final @Serial long serialVersionUID = -8459574127145976066L;

    @Id
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private UUID id;
}
