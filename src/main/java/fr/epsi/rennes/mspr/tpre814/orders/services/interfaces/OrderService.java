package fr.epsi.rennes.mspr.tpre814.orders.services.interfaces;

import fr.epsi.rennes.mspr.tpre814.orders.api.interfaces.CRUDInterface;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.shared.models.OrderDTO;

import java.util.List;
import java.util.UUID;

public interface OrderService extends CRUDInterface<Order, UUID, OrderDTO> {
    /**
     * Cancel an order<br>
     * If the order has been paid, the order will be refunded<br>
     * If the order has been sendProducts on stock, the order will be cancelled and the stock will be increased<br>
     *
     * <ul><h2>  If some Products have been shipped:</h2><br>
     * <li>- The order will be cancelled</li><br>
     * <li>- Send message to the delivery service to cancel the delivery</li><br>
     * <li>- Send message to the payment service to refund the order</li><br>
     * <li>- Restock any unsent products</li><br>
     * <li>- Place an Observer on the order to check if the delivery has been cancelled and track Products until returned</li><br>
     *     <li>- Then, restock the products that arrived</li><br>
     * <li>- Place the order on status CANCELLED</li><br>
     *
     * @param orderId the order to cancel
     * @return the paid order
     */
    void cancel(UUID orderId);

    /**
     * Closes an Order (it wil no longer be visible by users, but will be kept in the database for future references)<br>
     * <p>
     * When an Order is paid and Products are ALL Shipped, the order will be placed on status CLOSED<br>
     *
     * @param order the order to close
     * @return the closed order
     */
    boolean close(Order order);

    /**
     * Archive an Order<br>
     * When an Order has been closed for a time, it will be archived<br>
     *
     * @param order the order to archive
     * @return the archived order
     */
    boolean archive(Order order);


    List<OrderDTO>  getAllRequested(List<UUID> requested);

    List<OrderDTO> getCustomerOrders(UUID customerId);
}
