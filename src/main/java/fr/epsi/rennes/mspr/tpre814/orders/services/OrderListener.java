package fr.epsi.rennes.mspr.tpre814.orders.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.epsi.rennes.mspr.tpre814.orders.services.interfaces.OrderService;
import fr.epsi.rennes.mspr.tpre814.shared.events.OrderResponseEvent;
import fr.epsi.rennes.mspr.tpre814.shared.models.OrderDTO;
import jakarta.validation.constraints.NotEmpty;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static fr.epsi.rennes.mspr.tpre814.config.KafkaConfig.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderListener {
    private final OrderService orderService;
    private final KafkaTemplate<UUID, String> anyTemplate;

    @KafkaListener(topics = ORDER_REQUEST, groupId = SERVICE_KAFKA_GROUP,
            autoStartup = "true", info = "Order Service Listener",
            concurrency = "2")
    public void listenOrderRequests(@RequestBody String req,
                                    @Header(KafkaHeaders.RECEIVED_KEY) UUID key) throws JsonProcessingException {
        log.info("req ID : {}", key);
        log.info("Received Orders Request => {}", req);
        List<UUID> requested = parse(req);
        log.info("Requested Orders => {}", requested);
        List<OrderDTO> orders = orderService.getAllRequested(requested);
        OrderResponseEvent response = new OrderResponseEvent(key, orders);
        log.info("Response => {}", response);
        anyTemplate.send(ORDER_RESPONSE, key, response.toJson());
    }

    private List<UUID> parse(@NonNull@NotEmpty String req) {
        List<UUID> requested = new ArrayList<>();
        try {
            // Only one element
            requested.add(UUID.fromString(req.substring(1, req.length() - 1)));
            return requested;
        } catch (IllegalArgumentException e) {
            // Multiple elements
            String[] elements = req.substring(1, req.length() - 1).split(",");
            return Arrays.stream(elements).map(String::trim).map(String::strip).map(UUID::fromString).toList();
        }

    }

}
