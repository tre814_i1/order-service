package fr.epsi.rennes.mspr.tpre814.orders.errors;

public class OrderStatusException extends RuntimeException {
    private static final String MESSAGE = "Order status is not valid : ";

    public OrderStatusException(String s) {
        super(MESSAGE + s);
    }
}
