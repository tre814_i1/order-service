package fr.epsi.rennes.mspr.tpre814.orders.database.entity;

import fr.epsi.rennes.mspr.tpre814.orders.database.base.BaseEntityAudit;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderStatusException;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderValidationError;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OutOfStockException;
import fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus;
import fr.epsi.rennes.mspr.tpre814.shared.models.ProductDetails;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * From mock : <a href="https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/11/orders/3">Order Mock</a>
 * {
 * "createdAt": "2023-08-30T08:35:27.416Z",
 * "id": "3",
 * "customerId": "11",
 * "products": [
 * {
 * "createdAt": "2023-08-30T03:16:28.440Z",
 * "name": "Rufus Pacocha",
 * "details": {
 * "price": "97.00",
 * "description": "The Football Is Good For Training And Recreational Purposes",
 * "color": "red"
 * },
 * "stock": 73253,
 * "id": "3",
 * "orderId": "3"
 * },
 * {
 * "createdAt": "2023-08-29T22:55:14.728Z",
 * "name": "Paula Hodkiewicz",
 * "details": {
 * "price": "139.00",
 * "description": "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients",
 * "color": "salmon"
 * },
 * "stock": 1609,
 * "id": "53",
 * "orderId": "3"
 * }
 * ]
 * }
 */
@EqualsAndHashCode(callSuper = true)
@Slf4j
@EnableJpaAuditing
@Entity(name = "OrderEntity")
@Table(name = "orders")
@Getter
@Setter
public class Order extends BaseEntityAudit
        implements Serializable {
    private static final @Serial long serialVersionUID = 707624103562279911L;

    // Everytime status is updated, the updatedAt field will be updated
    // Also, we must keep the history or status changes for analytics
    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private @NotNull OrderStatus status;

    @Column(nullable = false)
    private UUID customerId;
    @Column(nullable = false, columnDefinition = "DECIMAL(15,2)")
    private double totalPrice;

    // Relationships
//    @JsonAlias("products")
    @OneToMany(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            targetEntity = OrderItem.class
    )
    private List<OrderItem> products;

    private UUID deliveryAddress;
    private String warn = "";

    private transient List<ProductDetails> details;
    public Order() {
        // Default constructor
        super();
    }

    public void touchValidations(boolean isNew) {
        if (isNew && this.getId() != null || !isNew && this.getId() == null) {
            throw new OrderValidationError(this, new IllegalStateException("Order id must %s null".formatted(isNew ? "be" : "not be")));
        }
        if (getProducts().isEmpty()) {
            throw new OrderValidationError(this, new IllegalStateException(getId() + " has no products"));
        }
        if (getCustomerId() == null) {
            throw new OrderValidationError(this, new IllegalStateException(getId() + " has no customer id"));
        }
    }

    public void validate() {
        this.touchValidations(false);
        if (deliveryAddress == null) {
            throw new OrderValidationError(this, new IllegalStateException("Order has no delivery address"));
        }
        if (products == null || products.isEmpty()) {
            throw new OrderValidationError(this, new IllegalStateException("Order has no products"));
        }
        if (totalPrice == 0) {
            throw new OutOfStockException(this, new IllegalStateException("Order has no total price, but products are present : %s".formatted(products)));
        }
        if (status == null) {
            throw new OrderStatusException("Order status must be not null");
        }
    }

    @Override
    public String toString() {
        String items = getProducts().stream().map(OrderItem::toString).collect(Collectors.joining(", "));
        return "{" +
                "id=" + getId() +
                ", status=" + status +
                ", customerId=" + customerId +
                ", totalPrice=" + totalPrice +
                ", orderItems=" + (items.isEmpty() ? "[]" : items) +
                ", deliveryAddress=" + deliveryAddress +
                ", warn='" + warn + "'" +
                '}';
    }

    public void setWarn(String warn) {
        if (this.warn.contains(warn)) {
            return;
        }
        log.warn("warn {} : {}", getId(), warn);
        this.warn = (this.warn.isEmpty() ? "" : this.warn + "<br>") + warn;
    }

    public List<OrderItem> getProducts() {
        if (products == null) {
            products = new ArrayList<>();
        }
        return products;
    }
}