package fr.epsi.rennes.mspr.tpre814.orders.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.orders.utils.OrderItemMapper;
import fr.epsi.rennes.mspr.tpre814.orders.utils.OrderMapper;
import fr.epsi.rennes.mspr.tpre814.shared.events.*;
import fr.epsi.rennes.mspr.tpre814.shared.utils.ItemMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static fr.epsi.rennes.mspr.tpre814.config.KafkaConfig.*;
import static fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus.READY;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaService extends AwaitTable {
    private final KafkaTemplate<UUID, String> anyTemplate;
    /**
     * Aggregate the results of requested services
     * /!!\ Must be used AFTER the kafka calls are done and joined !
     *
     * @param initOrder the order to update
     * @return the updated order to be saved
     * If anything went poorly but not critically, the resulting order will be returned
     * with param `warn` containing the error messages
     */
    public Order aggregateResults(Order initOrder) {
        OrderMapper.updateOrderProductsFromStockEvent(initOrder, getResponse(initOrder.getId()));
        OrderMapper.updateCustomer(initOrder, getResponse(initOrder.getCustomerId()));
        initOrder.setStatus(READY);
        initOrder.validate();
        return initOrder;
    }

    @Async
    public CompletableFuture<BaseEvent> getProducts(UUID key, List<UUID> list) {
        log.info("Requesting products {}", list);
        CompletableFuture<BaseEvent> fut = newRequest(key);
        log.debug("PRODUCTS JSON REQUEST :" + list.stream().map(UUID::toString).toList());
        anyTemplate.send(PRODUCTS_REQUEST, key, list.stream().map(UUID::toString).toList().toString());
        return fut;
    }

    @Async
    public CompletableFuture<BaseEvent> requestCustomer(UUID customerId, UUID orderId) {
        log.info("Requesting customer {}", customerId);
        CompletableFuture<BaseEvent> fut = newRequest(customerId);
        CustomerRequest request = new CustomerRequest(customerId, orderId);
        log.debug("CUSTOMER JSON REQUEST :" + request.toJson());
        anyTemplate.send(CUSTOMER_REQUEST, customerId, request.toJson());
        return fut;
    }

    @Async
    public CompletableFuture<BaseEvent> reserveProducts(Order newOrder) {
        log.info("Requesting stock subtraction for items {}", newOrder.getProducts());
        CompletableFuture<BaseEvent> fut = newRequest(newOrder.getId());
        StockRequest stockRequest = new StockRequest(newOrder.getId(), ItemMapper.toProductRequest(OrderItemMapper.toProducts(newOrder.getProducts())));
        stockRequest.setOp(StockOperation.SUB);
        log.debug("RESERVE JSON REQUEST :" + stockRequest.toJson());
        anyTemplate.send(RESERVE_TOPIC, newOrder.getId(), stockRequest.toJson());
        return fut;
    }

    //##############################################################################################################
    //################################################ LISTENERS ##################################################
    //##############################################################################################################

    /**
     * Await for kafka responses
     *
     * @param response the Customer to listenOrderRequests, received from the customer service
     */
    @KafkaListener(topics = CUSTOMER_RESPONSE, groupId = SERVICE_KAFKA_GROUP)
    public void listenCustomer(@RequestBody String response,
                               @Header(KafkaHeaders.RECEIVED_KEY) UUID key) throws JsonProcessingException {
        log.info("Customer response received for {}", key);
        log.debug("Received Customer => {}", response);
        CustomerResponse responseEvent;
        responseEvent = mapper.readValue(response, CustomerResponse.class);
        getPending(responseEvent.getId()).complete(responseEvent);
    }

    /**
     * Await for kafka responses
     *
     * @param event the products to listenOrderRequests, received from the stock service
     */
    @KafkaListener(topics = STOCK_RESPONSE, groupId = SERVICE_KAFKA_GROUP)
    public void listenStock(String event,
                            @Header(KafkaHeaders.RECEIVED_KEY) UUID key) throws JsonProcessingException {
        log.info("Stock response received for {}", key);
        log.debug("Received Stock => {}", event);
        StockResponse stockEvent;
        stockEvent = mapper.readValue(event, StockResponse.class);
        getPending(stockEvent.getId()).complete(stockEvent);
    }
}