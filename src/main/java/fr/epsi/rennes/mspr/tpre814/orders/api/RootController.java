package fr.epsi.rennes.mspr.tpre814.orders.api;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// disable swagger
@Hidden
@RestController
@RequestMapping("/api/v2/")
public class RootController {

    @RequestMapping("/ping")
    public HttpStatus ping() {
        return HttpStatus.OK;
    }
}
