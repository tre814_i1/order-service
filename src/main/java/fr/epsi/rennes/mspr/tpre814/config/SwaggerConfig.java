package fr.epsi.rennes.mspr.tpre814.config;

// springdoc-openapi-starter-webmvc-ui

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

// to ignore a given controller:
// @Hidden
@EnableSpringDataWebSupport
@Configuration
@OpenAPIDefinition(
        info = @Info(
                title = "TPRE814 API",
                version = "0.6.6",
                description = "Documentation for the TPRE814 Order-Service API" +
                        " [Part of the TPRE814 Micro-Service project]:" +
                        " gitlab.com/epsi-mspr/tpre814/",
                contact = @Contact(
                        name = "TPRE814 Team"),
                license = @License(
                        name = "Apache 2.0",
                        url = "https://www.apache.org/licenses/LICENSE-2.0.html"
                )
        )
)
@SecurityScheme(
        name = "bearerAuth",
        type = SecuritySchemeType.APIKEY,
        scheme = "bearer"
)
/**
 * Swagger is accessible at /swagger-ui.html
 */
public class SwaggerConfig {
}
