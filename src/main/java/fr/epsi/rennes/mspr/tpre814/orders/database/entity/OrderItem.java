package fr.epsi.rennes.mspr.tpre814.orders.database.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "order_item")
public class OrderItem implements Serializable {
    private static final @Serial long serialVersionUID = -7409786442773105329L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderItemId;

    @ManyToOne
    private Order order;
    private UUID id;
    // located in details.quantity
    private Integer quantity;
    private transient Integer stock;

    // located in details.price
    private Double price;

    @Override
    public String toString() {
        return String.format("{id=%s, quantity=%s, price=%s}", id, quantity, price);
    }

    public String toJson() {
        return "{\"id\":\"%s\",\"quantity\":\"%s\",\"price\":\"%s\"}"
                .formatted(id, quantity, price);
    }
}

