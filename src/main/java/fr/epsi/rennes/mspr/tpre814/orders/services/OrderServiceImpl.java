package fr.epsi.rennes.mspr.tpre814.orders.services;

import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.OrderHistory;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.OrderItem;
import fr.epsi.rennes.mspr.tpre814.orders.database.repository.OrderHistoryRepository;
import fr.epsi.rennes.mspr.tpre814.orders.database.repository.OrderRepository;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderDeletionException;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderNotFoundException;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderStatusException;
import fr.epsi.rennes.mspr.tpre814.orders.services.interfaces.OrderService;
import fr.epsi.rennes.mspr.tpre814.orders.utils.OrderMapper;
import fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus;
import fr.epsi.rennes.mspr.tpre814.shared.events.BaseEvent;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockResponse;
import fr.epsi.rennes.mspr.tpre814.shared.models.OrderDTO;
import jakarta.annotation.PostConstruct;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.ServiceUnavailableException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static fr.epsi.rennes.mspr.tpre814.config.KafkaConfig.TX_TIMEOUT;
import static fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus.*;

@Slf4j
@Service
@RequiredArgsConstructor
class OrderServiceImpl implements OrderService {
    private static final List<OrderStatus> validForChanges = List.of(OrderStatus.INIT, OrderStatus.RESERVED, OrderStatus.CONFIRMED, OrderStatus.READY);
    private static final List<OrderStatus> invalidForDeletion = List.of(OrderStatus.CLOSED, OrderStatus.ARCHIVED);
    private final KafkaService kafkaService;
    private final OrderRepository orderRepository;
    private final OrderHistoryRepository historyRepository;

    private static boolean checkStatus(@NonNull Order order, List<OrderStatus> statuses) {
        log.debug("Checking order status {}", order.getId());
        for (OrderStatus status : statuses) {
            if (order.getStatus() == status) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if an order is editable
     *
     * @param order the order to check
     *              if the order is not editable, an exception will be thrown
     * @throws OrderStatusException     if the order is not editable
     * @throws IllegalArgumentException if the order is null
     */
    static void checkIsEditable(Order order) {
        if (!checkStatus(order, validForChanges)) {
            throw new OrderStatusException(order.getId() + " cannot be updated -> " + order.getStatus().name());
        }
    }

    /**
     * Check if an order is deletable
     *
     * @param order the order to check
     *              if the order is not deletable (cancellable), an exception will be thrown
     * @throws OrderDeletionException   if the order is not deletable
     * @throws IllegalArgumentException if the order is null
     */
    static void checkCanBeCancelled(Order order) {
        if (checkStatus(order, invalidForDeletion)) {
            throw new OrderDeletionException(order.getId() + " cannot be deleted -> " + order.getStatus().name());
        }
    }

    @PostConstruct
    void setUp() {
        log.info("Order Service started");
    }

    @Override
    public OrderDTO get(UUID uuid) {
        log.debug("Getting order {}", uuid);
        Order order = getEntity(uuid);
        CompletableFuture<BaseEvent> fut = kafkaService.getProducts(order.getId(), order.getProducts().stream().map(OrderItem::getId).toList());
        log.debug("Order {} found", uuid);
        OrderDTO orderDTO = OrderMapper.toDTO(order);

        OrderMapper.appendProductDetails(orderDTO, ((StockResponse) fut.join()).getPayload());
        log.info("Items : {}", order.getProducts());
        return orderDTO;
    }

    private Order getEntity(@NonNull UUID uuid) {
        return orderRepository.findById(uuid).orElseThrow(() -> new OrderNotFoundException("Order " + uuid + " not found"));
    }

    @Override
    public List<OrderDTO> all() {
        return OrderMapper.toDTO(orderRepository.findAll());
    }

    /**
     * Update an order<br>
     * The order can be updated only if it is in status INIT or READY<br>
     * If the status is forced, it will be changed (the above statement is true)<br>
     *
     * @param entity the order to update
     * @return the updated order
     */
    @Override
    public OrderDTO update(Order entity) {
        log.info("Updating order {}", entity.getId());
        Order dbOrder = getEntity(entity.getId());
        checkIsEditable(dbOrder);
        Order result = OrderMapper.updateOrder(dbOrder, entity);

        if (entity.getStatus() != dbOrder.getStatus()) {
            // status has been forced, change it
            result = changeStatus(result.getId(), entity.getStatus());
        } else {
            result = save(result);
        }
        result.setProducts(entity.getProducts());
        return OrderMapper.toDTO(result);
    }

    @Override
    public void delete(UUID uuid) {
        cancel(uuid);
    }

    /**
     * Cancel an order<br>
     *
     * @param orderId the order to pay
     */
    @Override
    public void cancel(UUID orderId) {
        Order dbOrder = getEntity(orderId);
        // validate that the order can be deleted
        checkCanBeCancelled(dbOrder);

        // cancel the order, starting a new transaction
        changeStatus(dbOrder.getId(), CANCELLED);
        // place an observer on the order to check if the delivery has been cancelled and track Products until returned
        // cancel the delivery
        // restock the products
        // refund the order
        //...
    }

    /**
     * Closes an Order (it wil no longer be visible by users, but will be kept in the database for future references)<br>
     * When an Order is paid and Products are ALL Shipped, the order will be placed on status CLOSED<br>
     *
     * @param order the order to close
     * @return the closed order
     */
    @Override
    public boolean close(Order order) {
        if (order.getStatus() != OrderStatus.CONFIRMED) {
            throw new OrderStatusException("Order " + order.getId() + " cannot be closed : " + order.getStatus().name());
        }
        changeStatus(order.getId(), CLOSED);
        return true;
    }

    @Override
    public boolean archive(Order order) {
        if (!order.getStatus().equals(CLOSED)) {
            throw new OrderStatusException(order.getId() + " cannot be archived : " + order.getStatus().name());
        }
        changeStatus(order.getId(), ARCHIVED);
        return true;
    }

    @Override
    public List<OrderDTO> getAllRequested(List<UUID> requested) {
        return requested.isEmpty() ? List.of() :
                requested.stream()
                        .map(this::getEntity)
                        .map(OrderMapper::toDTO)
                        .toList();
    }


    private void historize(@NonNull UUID id, @NonNull OrderStatus orderStatus) {
        log.info("Historizing order {} with status {}", id, orderStatus);
        historyRepository.save(new OrderHistory(id, orderStatus));
    }

    private Order save(@NonNull Order order) {
        Order dbOrder = orderRepository.save(order);
        historize(dbOrder.getId(), dbOrder.getStatus());
        log.info("Order {} saved with status {}", dbOrder.getId(), dbOrder.getStatus());
        return dbOrder;
    }

    /**
     * Create a simili-blank Order in database<br>
     */
    private Order initializeOrder(@NonNull Order order) {
        log.debug("Touching order {}", order);
        order.touchValidations(true);
        Order touchOrder = new Order();
        touchOrder.setId(UUID.randomUUID());
        touchOrder.setStatus(INIT);
        touchOrder.setCustomerId(order.getCustomerId());
        Order dbOrder = save(touchOrder);
        dbOrder.setProducts(order.getProducts());
        return dbOrder;
    }

    private Order changeStatus(@NonNull UUID orderId, @NonNull OrderStatus status) {
        log.info("Changing status of order {} to {}", orderId, status);
        Order orderRef = getEntity(orderId);
        orderRef.setStatus(status);
        return save(orderRef);
    }

    @Override
    public List<OrderDTO> getCustomerOrders(UUID customerId) {
        return OrderMapper.toDTO(orderRepository.findByCustomerId(customerId));
    }

    //##############################################################################################################
    //################################################ PUBLIC CRUD #################################################
    //##############################################################################################################

    /**
     * Create a simili-blank Order to be filled with other services future responses<br>
     * In order to do so, the Order will be placed on status INIT by default,
     * meaning that it is not validated yet either on Stock or Customer identity
     *
     * @param newOrder the order to create on pending status
     * @return the created order
     */
    @SneakyThrows
    @Override
    @Transactional(value = "transactionManager",
            timeout = TX_TIMEOUT,
            rollbackFor = {ExecutionException.class, InterruptedException.class,
                           TimeoutException.class, ServiceUnavailableException.class},
            propagation = Propagation.REQUIRED)
    public OrderDTO create(Order newOrder) {
        log.info("Creating new order {}", newOrder.toString());
        // Order Request (do not save items to database, but keep them in memory for validations)
        Order initOrder = initializeOrder(newOrder);
        log.debug("Order {} created", initOrder.getId());

        // Request Customer and Reserve Products
        CompletableFuture.allOf(
                kafkaService.requestCustomer(initOrder.getCustomerId(), initOrder.getId()),
                kafkaService.reserveProducts(initOrder)
        ).join();
        return update(kafkaService.aggregateResults(initOrder));
    }
}
