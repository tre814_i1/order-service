package fr.epsi.rennes.mspr.tpre814.orders.api;

import fr.epsi.rennes.mspr.tpre814.orders.errors.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.kafka.listener.KafkaBackoffException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Magic Rest API error handler
 */
@Slf4j
@RestControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(OrderNotFoundException.class)
    public ResponseEntity<String> handleOrderNotFoundException(OrderNotFoundException e) {
        log.error("Order not found", e);
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler({HttpMessageNotReadableException.class})
    public ResponseEntity<String> handleOrderValidationError(RuntimeException e) {
        if (e instanceof OrderValidationError)
            log.error("Order validation error", e);
        else
            log.error("Error Parsing HTTP Order", e);
        return ResponseEntity.badRequest().body(e.getMessage());
    }
    @ExceptionHandler({OutOfStockException.class})
    public ResponseEntity<String> handleOrderValidationError(OutOfStockException e) {
        log.error("Completely dry !! ", e);
        return ResponseEntity.unprocessableEntity().body(e.getMessage());
    }

    @ExceptionHandler({EnumConstantNotPresentException.class})
    public ResponseEntity<String> handleEnumConstantNotPresentException(EnumConstantNotPresentException e) {
        log.error("Something went wrong !", e);
        return ResponseEntity.badRequest().body("Invalid state : " + e.constantName());
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<String> handleNullPointerException(NullPointerException e) {
        log.error("Null pointer exception", e);
        return ResponseEntity.badRequest().body("Null pointer exception");
    }
    @ExceptionHandler({OrderStatusException.class, OrderDeletionException.class})
    public ResponseEntity<String> handleOrderStatusException(OrderStatusException e) {
        log.error("Order status error", e);
        return ResponseEntity.unprocessableEntity().body(e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(Exception e) {
        log.error("Internal server error", e);
        return ResponseEntity.internalServerError().body("Internal server error");
    }

    @ExceptionHandler(KafkaBackoffException.class)
    public ResponseEntity<String> handleKafkaBackoffException(KafkaBackoffException e) {
        log.error("Service Unavailable", e);
        return ResponseEntity.status(503).body("Service Unavailable %s".formatted(e.getMessage()));
    }
}
