package fr.epsi.rennes.mspr.tpre814.orders.errors;

import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;

public class OutOfStockException extends RuntimeException {

    public OutOfStockException(Order order, Throwable e) {
        super("Order validation error for order " + order.getId() + " : " + e.getMessage());
    }
}