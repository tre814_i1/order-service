package fr.epsi.rennes.mspr.tpre814.orders.database.entity;

import ch.qos.logback.core.util.COWArrayList;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderStatusException;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderValidationError;
import fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static fr.epsi.rennes.mspr.tpre814.orders.Stubs.newTestOrder;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class OrderDiffblueTest {
    /**
     * Method under test: {@link Order#touchValidations(boolean)}
     */
    @Test
    void testTouchValidations() {
        // Arrange, Act and Assert
        assertThrows(OrderValidationError.class, () -> (new Order()).touchValidations(true));
    }

    /**
     * Method under test: {@link Order#touchValidations(boolean)}
     */
    @Test
    void testTouchValidations2() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setDeliveryAddress(UUID.randomUUID());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        order.setCustomerId(null);
        order.setId(null);
        order.setProducts(null);

        // Act and Assert
        assertThrows(OrderValidationError.class, () -> order.touchValidations(false));
    }

    /**
     * Method under test: {@link Order#touchValidations(boolean)}
     */
    @Test
    void testTouchValidations3() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setDeliveryAddress(UUID.randomUUID());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        order.setCustomerId(null);
        order.setId(UUID.randomUUID());
        order.setProducts(null);

        // Act and Assert
        assertThrows(OrderValidationError.class, () -> order.touchValidations(false));
    }

    /**
     * Method under test: {@link Order#touchValidations(boolean)}
     */
    @Test
    void testTouchValidations4() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setDeliveryAddress(UUID.randomUUID());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        order.setCustomerId(null);
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());

        // Act and Assert
        assertThrows(OrderValidationError.class, () -> order.touchValidations(false));
    }

    /**
     * Method under test: {@link Order#touchValidations(boolean)}
     */
    @Test
    void testTouchValidations5() {
        // Arrange
        Order order = new Order();
        order.setId(UUID.randomUUID());

        // Act and Assert
        assertThrows(OrderValidationError.class, () -> order.touchValidations(true));
    }

    /**
     * Method under test: {@link Order#touchValidations(boolean)}
     */
    @Test
    void testTouchValidations6() {
        // Arrange
        COWArrayList<OrderItem> orderItems = mock(COWArrayList.class);
        when(orderItems.isEmpty()).thenReturn(false);

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setDeliveryAddress(UUID.randomUUID());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        order.setCustomerId(null);
        order.setId(UUID.randomUUID());
        order.setProducts(orderItems);

        // Act and Assert
        assertThrows(OrderValidationError.class, () -> order.touchValidations(false));
        verify(orderItems).isEmpty();
    }

    /**
     * Method under test: {@link Order#touchValidations(boolean)}
     */
    @Test
    void testTouchValidations7() {
        // Arrange
        COWArrayList<OrderItem> orderItems = mock(COWArrayList.class);
        Order order = new Order();
        when(orderItems.isEmpty()).thenThrow(new OrderValidationError(order, new Throwable()));

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        order2.setCustomerId(null);
        order2.setId(UUID.randomUUID());
        order2.setProducts(orderItems);

        // Act and Assert
        assertThrows(OrderValidationError.class, () -> order2.touchValidations(false));
        verify(orderItems).isEmpty();
    }

    /**
     * Method under test: {@link Order#validate()}
     */
    @Test
    void testValidate() {
        // Arrange, Act and Assert
        assertThrows(OrderValidationError.class, () -> (new Order()).validate());
    }

    /**
     * Method under test: {@link Order#validate()}
     */
    @Test
    void testValidateNoStatus() {
        // Arrange, Act and Assert
        Order order = newTestOrder();
        order.setId(UUID.randomUUID());
        order.setStatus(null);
        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setOrderItemId(1L);
        orderItem.setPrice(10.0d);
        orderItem.setQuantity(1);
        order.setProducts(List.of(orderItem));
        String error = assertThrows(OrderStatusException.class, order::validate).getMessage();
        assertTrue(error.contains("Order status must be not null"));
    }

    /**
     * Method under test: {@link Order#validate()}
     */
    @Test
    void testValidateNoAddress() {
        // Arrange, Act and Assert
        Order order = newTestOrder();
        order.setId(UUID.randomUUID());
        order.setStatus(OrderStatus.INIT);
        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setOrderItemId(1L);
        orderItem.setPrice(10.0d);
        orderItem.setQuantity(1);
        order.setProducts(List.of(orderItem));
        order.setDeliveryAddress(null);
        String error = assertThrows(OrderValidationError.class, order::validate).getMessage();
        assertTrue(error.contains("has no delivery address"));
    }

    /**
     * Method under test: {@link Order#validate()}
     */
    @Test
    void testValidate2() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        order.setDeliveryAddress(null);
        order.setTotalPrice(0.0d);
        order.setCustomerId(null);
        order.setId(UUID.randomUUID());
        order.setProducts(null);
        order.setStatus(null);

        // Act and Assert
        assertThrows(OrderValidationError.class, () -> order.validate());
    }

    /**
     * Method under test: {@link Order#validate()}
     */
    @Test
    void testValidate3() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        order.setDeliveryAddress(null);
        order.setTotalPrice(0.0d);
        order.setCustomerId(null);
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(null);

        // Act and Assert
        assertThrows(OrderValidationError.class, () -> order.validate());
    }

    /**
     * Method under test: {@link Order#validate()}
     */
    @Test
    void testValidate4() {
        // Arrange
        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setOrderItemId(1L);
        orderItem.setPrice(10.0d);
        orderItem.setQuantity(1);

        ArrayList<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(orderItem);

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        order.setDeliveryAddress(null);
        order.setTotalPrice(0.0d);
        order.setCustomerId(null);
        order.setId(UUID.randomUUID());
        order.setProducts(orderItems);
        order.setStatus(null);

        // Act and Assert
        assertThrows(OrderValidationError.class, () -> order.validate());
    }

    /**
     * Method under test: {@link Order#getProducts()}
     */
    @Test
    void testGetOrderItems() {
        // Arrange, Act and Assert
        assertTrue((new Order()).getProducts().isEmpty());
    }

    /**
     * Method under test: {@link Order#getProducts()}
     */
    @Test
    void testGetOrderItems2() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        ArrayList<OrderItem> orderItems = new ArrayList<>();
        order.setProducts(orderItems);

        // Act
        List<OrderItem> actualOrderItems = order.getProducts();

        // Assert
        assertTrue(actualOrderItems.isEmpty());
        assertSame(orderItems, actualOrderItems);
    }

    /**
     * Method under test: {@link Order#setWarn(String)}
     */
    @Test
    void testSetWarn() {
        // Arrange
        Order order = new Order();

        // Act
        order.setWarn("Warn");

        // Assert
        assertEquals("Warn", order.getWarn());
    }

    /**
     * Methods under test:
     * <ul>
     *   <li>default or parameterless constructor of {@link Order}
     *   <li>{@link Order#toString()}
     * </ul>
     */
    @Test
    void testGettersAndSetters() {
        // Arrange, Act and Assert
        assertEquals("{id=null, status=null, customerId=null, totalPrice=0.0, orderItems=[], deliveryAddress=null, warn=''}",
                (new Order()).toString());
    }
}
