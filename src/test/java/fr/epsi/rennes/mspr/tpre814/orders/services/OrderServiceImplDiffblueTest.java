package fr.epsi.rennes.mspr.tpre814.orders.services;

import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.OrderHistory;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.OrderItem;
import fr.epsi.rennes.mspr.tpre814.orders.database.repository.OrderHistoryRepository;
import fr.epsi.rennes.mspr.tpre814.orders.database.repository.OrderRepository;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderNotFoundException;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderStatusException;
import fr.epsi.rennes.mspr.tpre814.orders.utils.OrderItemMapper;
import fr.epsi.rennes.mspr.tpre814.orders.utils.OrderMapper;
import fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus;
import fr.epsi.rennes.mspr.tpre814.shared.events.CustomerResponse;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockResponse;
import fr.epsi.rennes.mspr.tpre814.shared.models.Address;
import fr.epsi.rennes.mspr.tpre814.shared.models.Customer;
import fr.epsi.rennes.mspr.tpre814.shared.models.OrderDTO;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import fr.epsi.rennes.mspr.tpre814.shared.models.ProductDetails;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static fr.epsi.rennes.mspr.tpre814.orders.Stubs.newTestOrder;
import static fr.epsi.rennes.mspr.tpre814.orders.Stubs.newTestProduct;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class OrderServiceImplDiffblueTest {
    private OrderRepository orderRepository;

    private KafkaService kafkaService;

    private OrderServiceImpl orderServiceImpl;

    @BeforeEach
    void setUp() {
        orderRepository = mock(OrderRepository.class);
        OrderHistoryRepository orderHistoryRepository = mock(OrderHistoryRepository.class);
        kafkaService = mock(KafkaService.class);
        orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, orderHistoryRepository);
    }

    /**
     * Method under test: {@link OrderServiceImpl#setUp()}
     */
    @Test
    void testSetUp() {
        // Arrange
        ProducerFactory producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);

        // Act
        (new OrderServiceImpl(new KafkaService(new KafkaTemplate<>(producerFactory)), mock(OrderRepository.class),
                mock(OrderHistoryRepository.class))).setUp();

        // Assert that nothing has changed
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#get(UUID)}
     */
    @Test
    void testGet() {
        // Arrange
        Product product = newTestProduct();

        Order order = newTestOrder();
        order.setProducts(OrderItemMapper.toOrderItems(List.of(product)));

        when(orderRepository.findById(Mockito.any())).thenReturn(Optional.of(order));

        StockResponse stockResponse = new StockResponse(order.getId(), List.of(product));

        when(kafkaService.getProducts(Mockito.any(), Mockito.any()))
                .thenReturn(CompletableFuture.completedFuture(stockResponse));
        // Act
        OrderDTO actualGetResult = orderServiceImpl.get(UUID.randomUUID());

        // Assert
        verify(orderRepository).findById(isA(UUID.class));
        verify(kafkaService).getProducts(isA(UUID.class), isA(List.class));

        assertThat(actualGetResult).usingRecursiveComparison()
                .ignoringFields("products.details.description")
                .ignoringFields("products.details.name")
                .ignoringFields("products.name")
                .isEqualTo(OrderMapper.toDTO(order));
    }

    /**
     * Method under test: {@link OrderServiceImpl#get(UUID)}
     */
    @Test
    void testGet2() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.findById(Mockito.any())).thenThrow(new OrderStatusException("Getting order {}"));
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository,
                mock(OrderHistoryRepository.class));

        // Act and Assert
        assertThrows(OrderStatusException.class, () -> orderServiceImpl.get(UUID.randomUUID()));
        verify(orderRepository).findById(isA(UUID.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#get(UUID)}
     */
    @Test
    void testGet3() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        OrderRepository orderRepository = mock(OrderRepository.class);
        Optional<Order> emptyResult = Optional.empty();
        when(orderRepository.findById(Mockito.any())).thenReturn(emptyResult);
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository,
                mock(OrderHistoryRepository.class));

        // Act and Assert
        assertThrows(OrderNotFoundException.class, () -> orderServiceImpl.get(UUID.randomUUID()));
        verify(orderRepository).findById(isA(UUID.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#all()}
     */
    @Test
    void testAll() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        OrderRepository orderRepository = mock(OrderRepository.class);
        ArrayList<Order> orderList = new ArrayList<>();
        when(orderRepository.findAll()).thenReturn(orderList);

        // Act
        List<OrderDTO> actualAllResult = (new OrderServiceImpl(kafkaService, orderRepository,
                mock(OrderHistoryRepository.class))).all();

        // Assert
        verify(orderRepository).findAll();
        verify(producerFactory).transactionCapable();
        assertTrue(actualAllResult.isEmpty());
        assertThat(actualAllResult).usingRecursiveComparison().isEqualTo(orderList);
    }

    /**
     * Method under test: {@link OrderServiceImpl#all()}
     */
    @Test
    void testAll2() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.findAll()).thenThrow(new OrderStatusException("foo"));

        // Act and Assert
        assertThrows(OrderStatusException.class,
                () -> (new OrderServiceImpl(kafkaService, orderRepository, mock(OrderHistoryRepository.class))).all());
        verify(orderRepository).findAll();
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#update(Order)}
     */
    @ParameterizedTest
    @CsvSource({"INIT, INIT", "RESERVED, RESERVED", "PARTIAL, PARTIAL", "CONFIRMED, CONFIRMED", "CANCELLED, CANCELLED",
            "READY, READY"})
    void testUpdate(OrderStatus status, OrderStatus expectedStatus) {
        // Arrange
        KafkaService kafkaService = new KafkaService(mock(KafkaTemplate.class));
        Product testProduct = newTestProduct();
        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setProducts(OrderItemMapper.toOrderItems(List.of(testProduct)));
        entity.setDetails(List.of(testProduct.getDetails()));
        entity.setStatus(status);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.save(Mockito.any())).thenReturn(entity);
        when(orderRepository.findById(Mockito.any())).thenReturn(Optional.of(entity));
        OrderHistoryRepository historyRepository = mock(OrderHistoryRepository.class);
        when(historyRepository.save(Mockito.any())).thenReturn(new OrderHistory());
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, historyRepository);

        // Act
        OrderDTO expectedResult = OrderMapper.toDTO(entity);
        entity.setStatus(expectedStatus);
        if (OrderStatus.PARTIAL.equals(status) || OrderStatus.CANCELLED.equals(status)) {
            assertThrows(OrderStatusException.class, () -> orderServiceImpl.update(entity));
        } else {
            OrderDTO actualUpdateResult = orderServiceImpl.update(entity);
            assertThat(actualUpdateResult).usingRecursiveComparison().isEqualTo(expectedResult);
            verify(orderRepository).save(isA(Order.class));
            verify(historyRepository).save(isA(OrderHistory.class));
        }
        // Assert
        verify(orderRepository).findById(isA(UUID.class));
        assertThat(expectedResult).usingRecursiveComparison().isEqualTo(OrderMapper.toDTO(entity));
    }

    /**
     * Method under test: {@link OrderServiceImpl#update(Order)}
     */
    @Test
    void testUpdate2() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        OrderHistoryRepository historyRepository = mock(OrderHistoryRepository.class);
        when(historyRepository.save(Mockito.any())).thenThrow(new OrderStatusException("Updating order {}"));
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, historyRepository);

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        // Act and Assert
        assertThrows(OrderStatusException.class, () -> orderServiceImpl.update(entity));
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(historyRepository).save(isA(OrderHistory.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#update(Order)}
     */
    @Test
    void testUpdate3() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Updating order {}");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        OrderHistoryRepository historyRepository = mock(OrderHistoryRepository.class);
        when(historyRepository.save(Mockito.any())).thenReturn(new OrderHistory());
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, historyRepository);

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        // Act
        OrderDTO actualUpdateResult = orderServiceImpl.update(entity);

        // Assert
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(historyRepository).save(isA(OrderHistory.class));
        verify(producerFactory).transactionCapable();
        assertThat(actualUpdateResult).usingRecursiveComparison().isEqualTo(OrderMapper.toDTO(order2));
    }

    /**
     * Method under test: {@link OrderServiceImpl#update(Order)}
     */
    @Test
    void testUpdate4() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        OrderHistoryRepository historyRepository = mock(OrderHistoryRepository.class);
        when(historyRepository.save(Mockito.any())).thenReturn(new OrderHistory());
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, historyRepository);

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        // Act
        OrderDTO actualUpdateResult = orderServiceImpl.update(entity);

        // Assert
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(historyRepository).save(isA(OrderHistory.class));
        verify(producerFactory).transactionCapable();
        assertThat(actualUpdateResult).usingRecursiveComparison().isEqualTo(OrderMapper.toDTO(order2));
    }

    /**
     * Method under test: {@link OrderServiceImpl#update(Order)}
     */
    @Test
    void testUpdate5() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        OrderRepository orderRepository = mock(OrderRepository.class);
        Optional<Order> emptyResult = Optional.empty();
        when(orderRepository.findById(Mockito.any())).thenReturn(emptyResult);
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository,
                mock(OrderHistoryRepository.class));

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        // Act and Assert
        assertThrows(OrderNotFoundException.class, () -> orderServiceImpl.update(entity));
        verify(orderRepository).findById(isA(UUID.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#update(Order)}
     */
    @Test
    void testUpdate6() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        OrderHistoryRepository historyRepository = mock(OrderHistoryRepository.class);
        when(historyRepository.save(Mockito.any())).thenReturn(new OrderHistory());
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, historyRepository);

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(null);
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        // Act
        OrderDTO actualUpdateResult = orderServiceImpl.update(entity);

        // Assert
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(historyRepository).save(isA(OrderHistory.class));
        verify(producerFactory).transactionCapable();
        assertThat(actualUpdateResult).usingRecursiveComparison().isEqualTo(OrderMapper.toDTO(order2));
    }

    /**
     * Method under test: {@link OrderServiceImpl#update(Order)}
     */
    @Test
    void testUpdate7() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        OrderHistoryRepository historyRepository = mock(OrderHistoryRepository.class);
        when(historyRepository.save(Mockito.any())).thenReturn(new OrderHistory());
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, historyRepository);

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(null);
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        // Act
        OrderDTO actualUpdateResult = orderServiceImpl.update(entity);

        // Assert
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(historyRepository).save(isA(OrderHistory.class));
        verify(producerFactory).transactionCapable();
        assertThat(actualUpdateResult).usingRecursiveComparison().isEqualTo(OrderMapper.toDTO(order2));
    }

    /**
     * Method under test: {@link OrderServiceImpl#update(Order)}
     */
    @Test
    void testUpdate8() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.READY);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        OrderHistoryRepository historyRepository = mock(OrderHistoryRepository.class);
        when(historyRepository.save(Mockito.any())).thenReturn(new OrderHistory());
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, historyRepository);

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(0.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        // Act
        OrderDTO actualUpdateResult = orderServiceImpl.update(entity);

        // Assert
        verify(orderRepository, times(2)).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(historyRepository).save(isA(OrderHistory.class));
        verify(producerFactory).transactionCapable();
        assertThat(actualUpdateResult).usingRecursiveComparison().isEqualTo(OrderMapper.toDTO(order2));
    }

    /**
     * Method under test: {@link OrderServiceImpl#update(Order)}
     */
    @Test
    void testUpdate_changeStatus() {

        // Arrange
        UUID orderId = UUID.randomUUID();
        Order dbOrder = mock(Order.class);
        when(dbOrder.getId()).thenReturn(orderId);
        when(dbOrder.getStatus()).thenReturn(OrderStatus.INIT);
        when(dbOrder.getProducts()).thenReturn(List.of(new OrderItem()));
        when(dbOrder.getCustomerId()).thenReturn(UUID.randomUUID());
        when(dbOrder.getTotalPrice()).thenReturn(10.0d);
        when(orderRepository.findById(Mockito.any())).thenReturn(Optional.of(dbOrder)).thenReturn(Optional.of(dbOrder));

        Order changedOrder = new Order();
        changedOrder.setId(orderId);
        changedOrder.setStatus(OrderStatus.CANCELLED);
        when(orderRepository.save(Mockito.any())).thenReturn(changedOrder);

        // Act
        OrderDTO actualUpdateResult = orderServiceImpl.update(changedOrder);

        // Assert
        verify(orderRepository, times(2)).findById(orderId);
        verify(orderRepository).save(isA(Order.class));
        assertThat(actualUpdateResult).usingRecursiveComparison()
                .ignoringFields("status", "warn")
                .isEqualTo(OrderMapper.toDTO(changedOrder));
        assertThat(actualUpdateResult.getStatus()).isEqualTo(OrderStatus.CANCELLED);
    }

    /**
     * Method under test: {@link OrderServiceImpl#update(Order)}
     */
    @Test
    void testUpdate9() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.findById(Mockito.any())).thenThrow(new OrderStatusException("foo"));
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository,
                mock(OrderHistoryRepository.class));

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Entity");
        entity.setDeliveryAddress(null);
        entity.setTotalPrice(0.0d);
        entity.setCustomerId(null);
        entity.setStatus(null);

        // Act and Assert
        assertThrows(OrderStatusException.class, () -> orderServiceImpl.update(entity));
        verify(orderRepository).findById(isA(UUID.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#delete(UUID)}
     */
    @Test
    void testDelete() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        OrderHistoryRepository historyRepository = mock(OrderHistoryRepository.class);
        when(historyRepository.save(Mockito.any())).thenReturn(new OrderHistory());
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, historyRepository);

        // Act
        orderServiceImpl.delete(UUID.randomUUID());

        // Assert
        verify(orderRepository, atLeast(1)).findById(Mockito.any());
        verify(orderRepository).save(isA(Order.class));
        verify(historyRepository).save(isA(OrderHistory.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#delete(UUID)}
     */
    @Test
    void testDelete2() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        OrderHistoryRepository historyRepository = mock(OrderHistoryRepository.class);
        when(historyRepository.save(Mockito.any())).thenThrow(new OrderStatusException("Getting order {}"));
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, historyRepository);

        // Act and Assert
        assertThrows(OrderStatusException.class, () -> orderServiceImpl.delete(UUID.randomUUID()));
        verify(orderRepository, atLeast(1)).findById(Mockito.any());
        verify(orderRepository).save(isA(Order.class));
        verify(historyRepository).save(isA(OrderHistory.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#delete(UUID)}
     */
    @Test
    void testDelete3() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        OrderRepository orderRepository = mock(OrderRepository.class);
        Optional<Order> emptyResult = Optional.empty();
        when(orderRepository.findById(Mockito.any())).thenReturn(emptyResult);
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository,
                mock(OrderHistoryRepository.class));

        // Act and Assert
        assertThrows(OrderNotFoundException.class, () -> orderServiceImpl.delete(UUID.randomUUID()));
        verify(orderRepository).findById(isA(UUID.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#cancel(UUID)}
     */
    @Test
    void testCancel() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        OrderHistoryRepository historyRepository = mock(OrderHistoryRepository.class);
        when(historyRepository.save(Mockito.any())).thenReturn(new OrderHistory());
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, historyRepository);

        // Act
        orderServiceImpl.cancel(UUID.randomUUID());

        // Assert
        verify(orderRepository, atLeast(1)).findById(Mockito.any());
        verify(orderRepository).save(isA(Order.class));
        verify(historyRepository).save(isA(OrderHistory.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#cancel(UUID)}
     */
    @Test
    void testCancel2() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        OrderHistoryRepository historyRepository = mock(OrderHistoryRepository.class);
        when(historyRepository.save(Mockito.any())).thenThrow(new OrderStatusException("Getting order {}"));
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, historyRepository);

        // Act and Assert
        assertThrows(OrderStatusException.class, () -> orderServiceImpl.cancel(UUID.randomUUID()));
        verify(orderRepository, atLeast(1)).findById(Mockito.any());
        verify(orderRepository).save(isA(Order.class));
        verify(historyRepository).save(isA(OrderHistory.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#cancel(UUID)}
     */
    @Test
    void testCancel3() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        OrderRepository orderRepository = mock(OrderRepository.class);
        Optional<Order> emptyResult = Optional.empty();
        when(orderRepository.findById(Mockito.any())).thenReturn(emptyResult);
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository,
                mock(OrderHistoryRepository.class));

        // Act and Assert
        assertThrows(OrderNotFoundException.class, () -> orderServiceImpl.cancel(UUID.randomUUID()));
        verify(orderRepository).findById(isA(UUID.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#close(Order)}
     */
    @Test
    void testClose() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(new KafkaService(new KafkaTemplate<>(producerFactory)),
                mock(OrderRepository.class), mock(OrderHistoryRepository.class));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");

        // Act and Assert
        assertThrows(OrderStatusException.class, () -> orderServiceImpl.close(order));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#close(Order)}
     */
    @Test
    void testClose2() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        OrderHistoryRepository historyRepository = mock(OrderHistoryRepository.class);
        when(historyRepository.save(Mockito.any())).thenReturn(new OrderHistory());
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, historyRepository);

        Order order3 = new Order();
        order3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order3.setCustomerId(UUID.randomUUID());
        order3.setDeliveryAddress(UUID.randomUUID());
        order3.setId(UUID.randomUUID());
        order3.setProducts(new ArrayList<>());
        order3.setStatus(OrderStatus.CONFIRMED);
        order3.setTotalPrice(10.0d);
        order3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setUpdatedBy("2020-03-01");
        order3.setWarn("Warn");

        // Act
        boolean actualCloseResult = orderServiceImpl.close(order3);

        // Assert
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(historyRepository).save(isA(OrderHistory.class));
        verify(producerFactory).transactionCapable();
        assertTrue(actualCloseResult);
    }

    /**
     * Method under test: {@link OrderServiceImpl#close(Order)}
     */
    @Test
    void testClose3() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        OrderHistoryRepository historyRepository = mock(OrderHistoryRepository.class);
        when(historyRepository.save(Mockito.any()))
                .thenThrow(new OrderStatusException("Changing status of order {} to {}"));
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, historyRepository);

        Order order3 = new Order();
        order3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order3.setCustomerId(UUID.randomUUID());
        order3.setDeliveryAddress(UUID.randomUUID());
        order3.setId(UUID.randomUUID());
        order3.setProducts(new ArrayList<>());
        order3.setStatus(OrderStatus.CONFIRMED);
        order3.setTotalPrice(10.0d);
        order3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setUpdatedBy("2020-03-01");
        order3.setWarn("Warn");

        // Act and Assert
        assertThrows(OrderStatusException.class, () -> orderServiceImpl.close(order3));
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(historyRepository).save(isA(OrderHistory.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#close(Order)}
     */
    @Test
    void testClose4() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        OrderRepository orderRepository = mock(OrderRepository.class);
        Optional<Order> emptyResult = Optional.empty();
        when(orderRepository.findById(Mockito.any())).thenReturn(emptyResult);
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository,
                mock(OrderHistoryRepository.class));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.CONFIRMED);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");

        // Act and Assert
        assertThrows(OrderNotFoundException.class, () -> orderServiceImpl.close(order));
        verify(orderRepository).findById(isA(UUID.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#archive(Order)}
     */
    @Test
    void testArchive() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(new KafkaService(new KafkaTemplate<>(producerFactory)),
                mock(OrderRepository.class), mock(OrderHistoryRepository.class));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");

        // Act and Assert
        assertThrows(OrderStatusException.class, () -> orderServiceImpl.archive(order));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#archive(Order)}
     */
    @Test
    void testArchive2() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        OrderHistoryRepository historyRepository = mock(OrderHistoryRepository.class);
        when(historyRepository.save(Mockito.any())).thenReturn(new OrderHistory());
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, historyRepository);

        Order order3 = new Order();
        order3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order3.setCustomerId(UUID.randomUUID());
        order3.setDeliveryAddress(UUID.randomUUID());
        order3.setId(UUID.randomUUID());
        order3.setProducts(new ArrayList<>());
        order3.setStatus(OrderStatus.CLOSED);
        order3.setTotalPrice(10.0d);
        order3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setUpdatedBy("2020-03-01");
        order3.setWarn("Warn");

        // Act
        boolean actualArchiveResult = orderServiceImpl.archive(order3);

        // Assert
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(historyRepository).save(isA(OrderHistory.class));
        verify(producerFactory).transactionCapable();
        assertTrue(actualArchiveResult);
    }

    /**
     * Method under test: {@link OrderServiceImpl#archive(Order)}
     */
    @Test
    void testArchive3() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        OrderHistoryRepository historyRepository = mock(OrderHistoryRepository.class);
        when(historyRepository.save(Mockito.any()))
                .thenThrow(new OrderStatusException("Changing status of order {} to {}"));
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, historyRepository);

        Order order3 = new Order();
        order3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order3.setCustomerId(UUID.randomUUID());
        order3.setDeliveryAddress(UUID.randomUUID());
        order3.setId(UUID.randomUUID());
        order3.setProducts(new ArrayList<>());
        order3.setStatus(OrderStatus.CLOSED);
        order3.setTotalPrice(10.0d);
        order3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setUpdatedBy("2020-03-01");
        order3.setWarn("Warn");

        // Act and Assert
        assertThrows(OrderStatusException.class, () -> orderServiceImpl.archive(order3));
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(historyRepository).save(isA(OrderHistory.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#archive(Order)}
     */
    @Test
    void testArchive4() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        OrderRepository orderRepository = mock(OrderRepository.class);
        Optional<Order> emptyResult = Optional.empty();
        when(orderRepository.findById(Mockito.any())).thenReturn(emptyResult);
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository,
                mock(OrderHistoryRepository.class));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.CLOSED);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");

        // Act and Assert
        assertThrows(OrderNotFoundException.class, () -> orderServiceImpl.archive(order));
        verify(orderRepository).findById(isA(UUID.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#getAllRequested(List)}
     */
    @Test
    void testGetAllRequested() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(new KafkaService(new KafkaTemplate<>(producerFactory)),
                mock(OrderRepository.class), mock(OrderHistoryRepository.class));

        // Act
        List<OrderDTO> actualAllRequested = orderServiceImpl.getAllRequested(new ArrayList<>());

        // Assert
        verify(producerFactory).transactionCapable();
        assertTrue(actualAllRequested.isEmpty());
    }

    /**
     * Method under test: {@link OrderServiceImpl#getAllRequested(List)}
     */
    @Test
    void testGetAllRequested2() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        OrderRepository orderRepository = mock(OrderRepository.class);
        Optional<Order> emptyResult = Optional.empty();
        when(orderRepository.findById(Mockito.<UUID>any())).thenReturn(emptyResult);
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository,
                mock(OrderHistoryRepository.class));

        ArrayList<UUID> requested = new ArrayList<>();
        requested.add(UUID.randomUUID());

        // Act and Assert
        assertThrows(OrderNotFoundException.class, () -> orderServiceImpl.getAllRequested(requested));
        verify(orderRepository).findById(isA(UUID.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link OrderServiceImpl#getCustomerOrders(UUID)}
     */
    @Test
    void testGetCustomerOrders() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.findByCustomerId(Mockito.<UUID>any())).thenReturn(new ArrayList<>());
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository,
                mock(OrderHistoryRepository.class));

        // Act
        List<OrderDTO> actualCustomerOrders = orderServiceImpl.getCustomerOrders(UUID.randomUUID());

        // Assert
        verify(orderRepository).findByCustomerId(isA(UUID.class));
        verify(producerFactory).transactionCapable();
        assertTrue(actualCustomerOrders.isEmpty());
    }

    /**
     * Method under test: {@link OrderServiceImpl#getCustomerOrders(UUID)}
     */
    @Test
    void testGetCustomerOrders2() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setDetails(new ArrayList<>());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");

        ArrayList<Order> orderList = new ArrayList<>();
        orderList.add(order);
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.findByCustomerId(Mockito.<UUID>any())).thenReturn(orderList);
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository,
                mock(OrderHistoryRepository.class));

        // Act
        List<OrderDTO> actualCustomerOrders = orderServiceImpl.getCustomerOrders(UUID.randomUUID());

        // Assert
        verify(orderRepository).findByCustomerId(isA(UUID.class));
        verify(producerFactory).transactionCapable();
        assertEquals(1, actualCustomerOrders.size());
    }

    /**
     * Method under test: {@link OrderServiceImpl#getCustomerOrders(UUID)}
     */
    @Test
    void testGetCustomerOrders3() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setDetails(new ArrayList<>());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Created By");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setDetails(new ArrayList<>());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.RESERVED);
        order2.setTotalPrice(1.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020/03/01");
        order2.setWarn("");

        ArrayList<Order> orderList = new ArrayList<>();
        orderList.add(order2);
        orderList.add(order);
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.findByCustomerId(Mockito.<UUID>any())).thenReturn(orderList);
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository,
                mock(OrderHistoryRepository.class));

        // Act
        List<OrderDTO> actualCustomerOrders = orderServiceImpl.getCustomerOrders(UUID.randomUUID());

        // Assert
        verify(orderRepository).findByCustomerId(isA(UUID.class));
        verify(producerFactory).transactionCapable();
        assertEquals(2, actualCustomerOrders.size());
    }

    /**
     * Method under test: {@link OrderServiceImpl#getCustomerOrders(UUID)}
     */
    @Test
    void testGetCustomerOrders4() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setDetails(new ArrayList<>());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");

        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setOrder(order);
        orderItem.setOrderItemId(1L);
        orderItem.setPrice(10.0d);
        orderItem.setQuantity(1);
        orderItem.setStock(1);

        ArrayList<OrderItem> products = new ArrayList<>();
        products.add(orderItem);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setDetails(new ArrayList<>());
        order2.setId(UUID.randomUUID());
        order2.setProducts(products);
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");

        ArrayList<Order> orderList = new ArrayList<>();
        orderList.add(order2);
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.findByCustomerId(Mockito.<UUID>any())).thenReturn(orderList);
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository,
                mock(OrderHistoryRepository.class));

        // Act
        List<OrderDTO> actualCustomerOrders = orderServiceImpl.getCustomerOrders(UUID.randomUUID());

        // Assert
        verify(orderRepository).findByCustomerId(isA(UUID.class));
        verify(producerFactory).transactionCapable();
        assertEquals(1, actualCustomerOrders.size());
    }

    /**
     * Method under test: {@link OrderServiceImpl#getCustomerOrders(UUID)}
     */
    @Test
    void testGetCustomerOrders5() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        ArrayList<ProductDetails> details = new ArrayList<>();
        details.add(new ProductDetails("Name", "The characteristics of someone or something", 10.0d));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setDetails(new ArrayList<>());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");

        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setOrder(order);
        orderItem.setOrderItemId(1L);
        orderItem.setPrice(10.0d);
        orderItem.setQuantity(1);
        orderItem.setStock(1);

        ArrayList<OrderItem> products = new ArrayList<>();
        products.add(orderItem);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setDetails(details);
        order2.setId(UUID.randomUUID());
        order2.setProducts(products);
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");

        ArrayList<Order> orderList = new ArrayList<>();
        orderList.add(order2);
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.findByCustomerId(Mockito.<UUID>any())).thenReturn(orderList);
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository,
                mock(OrderHistoryRepository.class));

        // Act
        List<OrderDTO> actualCustomerOrders = orderServiceImpl.getCustomerOrders(UUID.randomUUID());

        // Assert
        verify(orderRepository).findByCustomerId(isA(UUID.class));
        verify(producerFactory).transactionCapable();
        assertEquals(1, actualCustomerOrders.size());
    }

    /**
     * Method under test: {@link OrderServiceImpl#create(Order)}
     */
    @Test
    void testCreate() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        OrderRepository orderRepository = mock(OrderRepository.class);
        when(orderRepository.save(Mockito.any())).thenReturn(order);
        OrderHistoryRepository historyRepository = mock(OrderHistoryRepository.class);
        when(historyRepository.save(Mockito.any())).thenThrow(new OrderStatusException("Creating new order {}"));
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafkaService, orderRepository, historyRepository);

        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setOrderItemId(2L);
        orderItem.setPrice(0.5d);
        orderItem.setQuantity(0);
        orderItem.setStock(0);

        ArrayList<OrderItem> products = new ArrayList<>();
        products.add(orderItem);

        Order newOrder = new Order();
        newOrder.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        newOrder.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        newOrder.setCustomerId(UUID.randomUUID());
        newOrder.setDeliveryAddress(UUID.randomUUID());
        newOrder.setId(null);
        newOrder.setProducts(products);
        newOrder.setStatus(OrderStatus.INIT);
        newOrder.setTotalPrice(10.0d);
        newOrder.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        newOrder.setUpdatedBy("2020-03-01");
        newOrder.setWarn("Warn");

        // Act and Assert
        assertThrows(OrderStatusException.class, () -> orderServiceImpl.create(newOrder));
        verify(orderRepository).save(isA(Order.class));
        verify(historyRepository).save(isA(OrderHistory.class));
        verify(producerFactory).transactionCapable();
    }

    @Test
    void testCreate_NOK() {
        KafkaService kafka = mock(KafkaService.class);
        OrderRepository orderRepository = mock(OrderRepository.class);
        OrderHistoryRepository orderHistoryRepository = mock(OrderHistoryRepository.class);
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafka, orderRepository, orderHistoryRepository);

        Order order = newTestOrder();
        order.setId(null);
        Customer customer = new Customer();
        customer.setId(UUID.randomUUID());
        customer.setAddress(new Address(UUID.randomUUID(), "street", "city", "zip", "country"));
        CustomerResponse customerResponse = new CustomerResponse(UUID.randomUUID());
        customerResponse.setPayload(customer);
        when(kafka.requestCustomer(Mockito.any(), Mockito.any()))
                .thenReturn(CompletableFuture.completedFuture(customerResponse));

        Product product1 = new Product();
        product1.setId(UUID.randomUUID());
        product1.getDetails().setPrice(10.0d);
        product1.setStock(10);
        product1.setQuantity(1);
        Product product2 = new Product();
        product2.setId(UUID.randomUUID());
        product2.getDetails().setPrice(20.0d);
        product2.setQuantity(2);

        order.getProducts().addAll(OrderItemMapper.toOrderItems(List.of(product1, product2)));

        StockResponse stockResponse = new StockResponse(UUID.randomUUID(), List.of(product1, product2));
        when(kafka.reserveProducts(Mockito.any())).thenReturn(CompletableFuture.completedFuture(stockResponse));

        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());
        // when we save the order, we have to set the Id, otherwise te method fails
        when(orderRepository.save(Mockito.any())).thenAnswer(invocation -> {
            Order order1 = invocation.getArgument(0);
            order1.setId(UUID.randomUUID());
            return order1;
        });
        when(orderRepository.findById(Mockito.any())).thenReturn(Optional.of(order));
        when(kafka.getResponse(Mockito.any())).thenReturn(customerResponse).thenReturn(stockResponse);
        when(kafka.aggregateResults(Mockito.any())).thenReturn(order);

        // Act
        assertThrows(NullPointerException.class, () -> orderServiceImpl.create(order));

        // Assert
        verify(kafka).requestCustomer(isA(UUID.class), isA(UUID.class));
        verify(kafka).reserveProducts(isA(Order.class));
    }

    /**
     * CompletableFuture.allOf(
     * kafkaService.requestCustomer(initOrder.getCustomerId()),
     * kafkaService.reserveProducts(initOrder) ).whenComplete((response, throwable)
     * -> { if (throwable != null) { log.error("Error while creating order {}",
     * initOrder.getId(), throwable); changeStatus(initOrder.getId(), CANCELLED);
     * throw new OrderStatusException("Error while creating order " +
     * initOrder.getId()); } }).join();
     */
    @Test
    void testCreateOrderFailsOnKafka() {
        // same as above, but we throw an exception when kafka fails on any call
        KafkaService kafka = mock(KafkaService.class);
        OrderRepository orderRepository = mock(OrderRepository.class);
        OrderHistoryRepository orderHistoryRepository = mock(OrderHistoryRepository.class);
        OrderServiceImpl orderServiceImpl = new OrderServiceImpl(kafka, orderRepository, orderHistoryRepository);

        Order order = newTestOrder();
        order.setId(null);
        Customer customer = new Customer();
        customer.setId(UUID.randomUUID());
        customer.setAddress(new Address(UUID.randomUUID(), "street", "city", "zip", "country"));
        CustomerResponse customerResponse = new CustomerResponse(UUID.randomUUID());
        customerResponse.setPayload(customer);
        when(kafka.requestCustomer(Mockito.any(), Mockito.any()))
                .thenReturn(CompletableFuture.completedFuture(customerResponse));

        Product product1 = new Product();
        product1.setId(UUID.randomUUID());
        product1.getDetails().setPrice(10.0d);
        product1.setStock(10);
        product1.setQuantity(1);
        Product product2 = new Product();
        product2.setId(UUID.randomUUID());
        product2.getDetails().setPrice(20.0d);
        product2.setQuantity(2);

        order.getProducts().addAll(OrderItemMapper.toOrderItems(List.of(product1, product2)));

        StockResponse stockResponse = new StockResponse(UUID.randomUUID(), List.of(product1, product2));
        when(kafka.reserveProducts(Mockito.any())).thenReturn(CompletableFuture.completedFuture(stockResponse));
        when(kafka.requestCustomer(Mockito.any(), Mockito.any())).thenThrow(new RuntimeException("Kafka error"));

        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());

        // when we save the order, we have to set the Id, otherwise te method fails
        when(orderRepository.save(Mockito.any())).thenAnswer(invocation -> {
            Order order1 = invocation.getArgument(0);
            order1.setId(UUID.randomUUID());
            return order1;
        });
        when(orderRepository.findById(Mockito.any())).thenReturn(Optional.of(order));
        when(kafka.getResponse(Mockito.any())).thenReturn(customerResponse).thenReturn(stockResponse);

        // Act and Assert
        assertThrows(RuntimeException.class, () -> orderServiceImpl.create(order));
    }
}
