package fr.epsi.rennes.mspr.tpre814.orders.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.OrderItem;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderDeletionException;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderStatusException;
import fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus;
import fr.epsi.rennes.mspr.tpre814.shared.events.BaseEvent;
import fr.epsi.rennes.mspr.tpre814.shared.events.CustomerResponse;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockResponse;
import fr.epsi.rennes.mspr.tpre814.shared.models.Address;
import fr.epsi.rennes.mspr.tpre814.shared.models.Customer;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import org.apache.kafka.clients.producer.MockProducer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.common.serialization.UUIDSerializer;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import javax.naming.ServiceUnavailableException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static fr.epsi.rennes.mspr.tpre814.orders.Stubs.newTestOrder;
import static fr.epsi.rennes.mspr.tpre814.orders.Stubs.newTestProduct;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class KafkaServiceDiffblueTest {
    /**
     * Method under test: {@link KafkaService#tearDown()}
     */
    @Test
    void testTearDown() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);

        // Act
        (new KafkaService(new KafkaTemplate<>(producerFactory))).tearDown();

        // Assert that nothing has changed
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link KafkaService#tearDown()}
     */
    @Test
    void testTearDown_withPendings() {
        // Arrange
        KafkaService kafkaService = new KafkaService(null);
        UUID id = UUID.randomUUID();
        CompletableFuture<BaseEvent> future = mock(CompletableFuture.class);
        kafkaService.pendingRequests.put(id, future);

        // Act
        kafkaService.tearDown();

        // Assert that nothing has changed
        // ensure future has been terminated
        verify(future).completeExceptionally(isA(ServiceUnavailableException.class));
    }

    /**
     * Method under test: {@link KafkaService#requestCustomer(UUID, UUID)}
     */
    @Test
    void testValidateCustomer() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        UUIDSerializer keySerializer = new UUIDSerializer();
        when(producerFactory.createProducer()).thenReturn(new MockProducer<>(true, keySerializer, new StringSerializer()));
        when(producerFactory.transactionCapable()).thenReturn(false);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        // Act
        kafkaService.requestCustomer(UUID.randomUUID(), UUID.randomUUID());

        // Assert
        verify(producerFactory).createProducer();
        verify(producerFactory).transactionCapable();
        assertEquals(1, kafkaService.pendingRequests.size());
    }

    /**
     * Method under test: {@link KafkaService#reserveProducts(Order)}
     */
    @Test
    void testReserveProducts() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        UUIDSerializer keySerializer = new UUIDSerializer();
        when(producerFactory.createProducer()).thenReturn(new MockProducer<>(true, keySerializer, new StringSerializer()));
        when(producerFactory.transactionCapable()).thenReturn(false);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Order newOrder = new Order();
        newOrder.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        newOrder.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        newOrder.setCustomerId(UUID.randomUUID());
        newOrder.setDeliveryAddress(UUID.randomUUID());
        newOrder.setId(UUID.randomUUID());
        newOrder.setProducts(new ArrayList<>());
        newOrder.setStatus(OrderStatus.INIT);
        newOrder.setTotalPrice(10.0d);
        newOrder.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        newOrder.setUpdatedBy("2020-03-01");
        newOrder.setWarn("Warn");

        // Act
        kafkaService.reserveProducts(newOrder);

        // Assert
        verify(producerFactory).createProducer();
        verify(producerFactory).transactionCapable();
        assertEquals(1, kafkaService.pendingRequests.size());
    }

    /**
     * Method under test: {@link KafkaService#listenCustomer(String, UUID)}
     */
    @Test
    void testListenCustomer_OK() throws JsonProcessingException {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        CustomerResponse customerResponse = new CustomerResponse(UUID.randomUUID());
        Customer customer = new Customer(UUID.randomUUID());
        customer.getCompany().setId(UUID.randomUUID());
        customer.getCompany().setAddress(new Address(UUID.randomUUID()));
        customer.getAddress().setId(UUID.randomUUID());
        customer.getProfile().setId(UUID.randomUUID());

        customerResponse.setPayload(customer);

        CompletableFuture<BaseEvent> future = new CompletableFuture<>();

        // Act
        // emulate the completion of the future
        kafkaService.pendingRequests.put(customerResponse.getId(), future);
        future.complete(customerResponse);
        kafkaService.listenCustomer(customerResponse.toJson(), customerResponse.getId());

        // Assert
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link KafkaService#listenCustomer(String, UUID)}
     */
    @Test
    void testListenCustomer2() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));

        Customer customer = new Customer();
        customer.setId(UUID.randomUUID());
        CustomerResponse customerResponse = new CustomerResponse(UUID.randomUUID());
        customerResponse.setPayload(customer);

        // Act
        assertThrows(IllegalStateException.class, () -> kafkaService.listenCustomer((new ObjectMapper()).writeValueAsString(customerResponse), customerResponse.getId()));
    }

    /**
     * Method under test: {@link KafkaService#listenStock(String, UUID)}
     */
    @Test
    void testListenStock_OK() throws JsonProcessingException {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        Product product = new Product();
        product.setId(UUID.randomUUID());
        StockResponse stockResponse = new StockResponse(UUID.randomUUID(), List.of(product));

        // emulate the completion of the future
        CompletableFuture<BaseEvent> future = mock(CompletableFuture.class);
        kafkaService.pendingRequests.put(stockResponse.getId(), future);
        when(future.complete(stockResponse)).thenReturn(true);

        // Act
        kafkaService.listenStock(stockResponse.toJson(), stockResponse.getId());

        // Assert
        verify(future).complete(any());

    }

    /**
     * Method under test: {@link KafkaService#listenStock(String, UUID)}
     */
    @Test
    void testListenStock_ButNotPreviouslyRequested() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        UUID id = UUID.randomUUID();

        // Act
        assertThrows(IllegalStateException.class, () -> kafkaService.listenStock(new StockResponse(id, new ArrayList<>()).toJson(), id));
        // Assert
    }

    @Test
    void testGetPending() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        UUID id = UUID.randomUUID();
        kafkaService.pendingRequests.put(id, new CompletableFuture<>());

        // Act
        kafkaService.getPending(id);

    }

    /**
     * @SneakyThrows public BaseEvent getResponse(UUID id) {
     * return getPending(id).get(10, java.util.concurrent.TimeUnit.SECONDS);
     * }
     */

    @Test
    void testGetResponse() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        UUID id = UUID.randomUUID();
        CompletableFuture<BaseEvent> future = new CompletableFuture<>();
        kafkaService.pendingRequests.put(id, future);

        // Act
        future.complete(new CustomerResponse(id));
        kafkaService.getResponse(id);

    }

    @Test
    void testGetResponse_NOK() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        KafkaService kafkaService = new KafkaService(new KafkaTemplate<>(producerFactory));
        UUID id = UUID.randomUUID();

        // Act
        assertThrows(IllegalStateException.class, () -> kafkaService.getResponse(id));

    }

    @Test
    void testAggregateResults() {
        // Arrange Order, Customer and Product
        KafkaService kafka = new KafkaService(null);
        Product product = newTestProduct();

        OrderItem orderItem = new OrderItem();
        orderItem.setId(product.getId());
        orderItem.setQuantity(2);

        Order order = newTestOrder();
        order.getProducts().add(orderItem);

        Customer customer = new Customer();
        customer.setId(order.getCustomerId());
        Address addr = new Address();
        addr.setId(UUID.randomUUID());
        customer.setAddress(addr);

        // Create the responses and futures

        CustomerResponse customerResponse = new CustomerResponse(order.getCustomerId());
        customerResponse.setPayload(customer);
        CompletableFuture<BaseEvent> futureCustomer = new CompletableFuture<>();
        kafka.pendingRequests.put(order.getCustomerId(), futureCustomer);

        StockResponse stockResponse = new StockResponse(order.getId(), List.of(product));
        CompletableFuture<BaseEvent> futureProducts = new CompletableFuture<>();
        kafka.pendingRequests.put(order.getId(), futureProducts);

        // Emulate the completion of the futures
        futureCustomer.complete(customerResponse);
        futureProducts.complete(stockResponse);

        // Act
        kafka.aggregateResults(order);

        // Assert that the order is correctly updated with services response
        assertEquals(OrderStatus.READY, order.getStatus());
        assertEquals(1, order.getProducts().size());
        assertEquals(product.getId(), order.getProducts().getFirst().getId());
        assertEquals(2, order.getProducts().getFirst().getQuantity());
        assertEquals(200.0, order.getTotalPrice());
    }

    @Test
    void testCheckIsEditable_Editable() {
        Order order = new Order();
        order.setStatus(OrderStatus.INIT);

        OrderServiceImpl.checkIsEditable(order);
    }

    @Test
    void testCheckIsEditable_NotEditable() {
        Order order = new Order();
        order.setStatus(OrderStatus.CLOSED);

        assertThrows(OrderStatusException.class, () -> {
            OrderServiceImpl.checkIsEditable(order);
        });
    }

    @Test
    void testCheckCanBeCancelled_Deletable() {
        Order order = new Order();
        order.setStatus(OrderStatus.INIT);

        OrderServiceImpl.checkCanBeCancelled(order);
    }

    @Test
    void testCheckCanBeCancelled_NotDeletable() {
        Order order = new Order();
        order.setStatus(OrderStatus.CLOSED);

        assertThrows(OrderDeletionException.class, () -> {
            OrderServiceImpl.checkCanBeCancelled(order);
        });
    }

    @Test
    void getProducts() {
        // Arrange
        KafkaTemplate<UUID, String> anyTemplate = mock(KafkaTemplate.class);
        KafkaService kafka = new KafkaService(anyTemplate);
        Order order = newTestOrder();

        // Act
        CompletableFuture<BaseEvent> ret = kafka.getProducts(order.getId(), List.of(UUID.randomUUID()));

        // Assert
        assertEquals(1, kafka.pendingRequests.size());
        assertEquals(order.getId(), kafka.pendingRequests.keySet().iterator().next());
        assertEquals(ret, kafka.pendingRequests.values().iterator().next());
    }
}
