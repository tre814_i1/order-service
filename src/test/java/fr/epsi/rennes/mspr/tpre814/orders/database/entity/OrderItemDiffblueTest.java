package fr.epsi.rennes.mspr.tpre814.orders.database.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OrderItemDiffblueTest {
    /**
     * Method under test: {@link OrderItem#toString()}
     */
    @Test
    void testToString() {
        // Arrange, Act and Assert
        assertEquals("{id=null, quantity=null, price=null}", (new OrderItem()).toString());
    }
}
