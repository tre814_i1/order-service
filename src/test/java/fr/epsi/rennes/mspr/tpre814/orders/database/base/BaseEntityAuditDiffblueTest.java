package fr.epsi.rennes.mspr.tpre814.orders.database.base;

import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class BaseEntityAuditDiffblueTest {
    /**
     * Method under test: {@link BaseEntityAudit#getCreatedAt()}
     */
    @Test
    void testGetCreatedAt() {
        // Arrange, Act and Assert
        assertNull((new Order()).getCreatedAt());
    }

    /**
     * Method under test: {@link BaseEntityAudit#getCreatedBy()}
     */
    @Test
    void testGetCreatedBy() {
        // Arrange, Act and Assert
        assertNull((new Order()).getCreatedBy());
    }

    /**
     * Method under test: {@link BaseEntityAudit#getUpdatedAt()}
     */
    @Test
    void testGetUpdatedAt() {
        // Arrange, Act and Assert
        assertNull((new Order()).getUpdatedAt());
    }

    /**
     * Method under test: {@link BaseEntityAudit#getUpdatedBy()}
     */
    @Test
    void testGetUpdatedBy() {
        // Arrange, Act and Assert
        assertNull((new Order()).getUpdatedBy());
    }

    /**
     * Method under test: {@link BaseEntityAudit#setCreatedAt(LocalDateTime)}
     */
    @Test
    void testSetCreatedAt() {
        // Arrange
        Order order = new Order();
        LocalDateTime createdAt = LocalDate.of(1970, 1, 1).atStartOfDay();

        // Act
        order.setCreatedAt(createdAt);

        // Assert
        assertSame(createdAt, order.getCreatedAt());
    }

    /**
     * Method under test: {@link BaseEntityAudit#setCreatedBy(String)}
     */
    @Test
    void testSetCreatedBy() {
        // Arrange
        Order order = new Order();

        // Act
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");

        // Assert
        assertEquals("Jan 1, 2020 8:00am GMT+0100", order.getCreatedBy());
    }

    /**
     * Method under test: {@link BaseEntityAudit#setUpdatedAt(LocalDateTime)}
     */
    @Test
    void testSetUpdatedAt() {
        // Arrange
        Order order = new Order();
        LocalDateTime updatedAt = LocalDate.of(1970, 1, 1).atStartOfDay();

        // Act
        order.setUpdatedAt(updatedAt);

        // Assert
        assertSame(updatedAt, order.getUpdatedAt());
    }

    /**
     * Method under test: {@link BaseEntityAudit#setUpdatedBy(String)}
     */
    @Test
    void testSetUpdatedBy() {
        // Arrange
        Order order = new Order();

        // Act
        order.setUpdatedBy("2020-03-01");

        // Assert
        assertEquals("2020-03-01", order.getUpdatedBy());
    }
}
