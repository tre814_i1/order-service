package fr.epsi.rennes.mspr.tpre814.orders.services;

import fr.epsi.rennes.mspr.tpre814.IntegrationDummyApplication;
import fr.epsi.rennes.mspr.tpre814.OrderManagerApplication;
import fr.epsi.rennes.mspr.tpre814.orders.api.FakeCustomerEventListener;
import fr.epsi.rennes.mspr.tpre814.orders.api.FakeStockEventListener;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.orders.database.repository.OrderHistoryRepository;
import fr.epsi.rennes.mspr.tpre814.orders.database.repository.OrderRepository;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderValidationError;
import fr.epsi.rennes.mspr.tpre814.orders.utils.OrderItemMapper;
import fr.epsi.rennes.mspr.tpre814.shared.models.Customer;
import fr.epsi.rennes.mspr.tpre814.shared.models.OrderDTO;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;
import java.util.UUID;

import static fr.epsi.rennes.mspr.tpre814.orders.Stubs.newTestOrder;
import static fr.epsi.rennes.mspr.tpre814.orders.Stubs.newTestProduct;
import static fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus.INIT;
import static fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus.READY;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.testcontainers.shaded.org.awaitility.Awaitility.await;

@ActiveProfiles("integration-test")
@TestPropertySource(
        properties = {
                "spring.kafka.consumer.auto-offset-reset=earliest",
        }
)
@Testcontainers
@AutoConfigureDataJpa
@RunWith(value = SpringRunner.class)
@SpringBootTest(classes = {IntegrationDummyApplication.class, OrderManagerApplication.class})
class ApplicationIT {
    @Autowired
    private FakeStockEventListener fakeStockEventListener;
    @Autowired
    private FakeCustomerEventListener fakeCustomerEventListener;
    @Autowired
    private OrderHistoryRepository orderHistoryRepository;
    @Autowired
    private OrderServiceImpl orderController;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderServiceImpl orderService;

    @DynamicPropertySource
    static void overrideProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.kafka.bootstrap-servers", IntegrationDummyApplication::getKafkaBootstrap);
        registry.add("spring.datasource.url", IntegrationDummyApplication::getPostgresUrl);
        registry.add("spring.datasource.username", IntegrationDummyApplication::getPostgresUsername);
        registry.add("spring.datasource.password", IntegrationDummyApplication::getPostgresPassword);
    }

    @BeforeAll
    static void init() {
        System.out.println("""
                #########################################################################################
                #########################################################################################
                        
                AcceptationTests
                        
                #########################################################################################
                #########################################################################################
                """);
        IntegrationDummyApplication.start();
    }

    @AfterAll
    static void destroy() {
        IntegrationDummyApplication.stop();
    }

    @AfterEach
    void tearDown() {
        orderHistoryRepository.deleteAll();
        orderRepository.deleteAll();
    }

    @Test
    void testConnectPostgres()
            throws Exception {
        Connection conn = IntegrationDummyApplication.getPostgresConnection();
        ResultSet resultSet = conn.createStatement().executeQuery("SELECT 1");
        resultSet.next();
        int result = resultSet.getInt(1);
        Assert.assertEquals(1, result);
    }

    @Test
    @Transactional
    void testDatabaseSaveInteraction() {
        // Given
        Customer customer = new Customer();
        customer.setId(UUID.randomUUID());

        Order order = new Order();
        order.setStatus(INIT);
        order.setId(UUID.fromString("118218df-4b3b-4b3b-8b3b-8b3b8b3b8b3b"));
        order.setTotalPrice(100.0);
        order.setCustomerId(customer.getId());

        // When
        UUID orderId = orderRepository.save(order).getId();

        // Then
        Order updatedOrder = orderRepository.getReferenceById(orderId);
        Assert.assertEquals(INIT, updatedOrder.getStatus());
    }

    @Test
    void acceptationTest_createOrder() {
        // Given / Arrange
        Product product = newTestProduct();
        Order order = newTestOrder();
        order.setId(null);
        order.setCustomerId(UUID.randomUUID());
        order.setProducts(OrderItemMapper.toOrderItems(List.of(product)));

        // When / Act
        OrderDTO actual = orderController.create(order);

        // Then / Assert
        // assert that kafka actually received the messages
        await().atMost(3, SECONDS).untilAsserted(() -> {
            assertEquals(1, fakeStockEventListener.getStockResponses().size());
            assertEquals(order.getCustomerId(), fakeCustomerEventListener.getCustomersOut().getId());
        });

        // assert that the order is saved and the history is updated
        assertEquals(2, orderHistoryRepository.count());
        assertEquals(1, orderRepository.count());

        // check the response content
        assertThat(actual).isNotNull();
        assertThat(actual.getId()).isNotNull();
        assertThat(actual.getCustomerId()).isEqualTo(order.getCustomerId());
        assertThat(actual.getStatus()).isEqualTo(READY);
        List<Product> products = fakeStockEventListener.getStockResponses().getFirst().getPayload();
        assertThat(products).hasSize(1);
        assertThat(products.getFirst().getId()).isEqualTo(product.getId());
    }

    @Test
    void acceptationTest_createOrderWithNoProducts() {
        // Given / Arrange
        Order order = new Order();
        order.setCustomerId(UUID.randomUUID());

        // When / Act
        assertThrows(OrderValidationError.class, () -> orderController.create(order));

        // Then / Assert
        assertEquals(0, orderRepository.count());
        assertEquals(0, orderHistoryRepository.count());
    }

    @Test
    void acceptationTest_createOrderWithNoCustomerId() {
        // Given / Arrange
        Product product = newTestProduct();
        Order order = new Order();
        order.setProducts(OrderItemMapper.toOrderItems(List.of(product)));

        // When / Act
        assertThrows(OrderValidationError.class, () -> orderController.create(order));

        // Then / Assert
        assertEquals(0, orderRepository.count());
    }
}
