package fr.epsi.rennes.mspr.tpre814;

import org.junit.jupiter.api.Test;
import org.springframework.web.context.support.StandardServletEnvironment;

import static org.junit.jupiter.api.Assertions.assertTrue;

class OrderManagerApplicationDiffblueTest {
    /**
     * Method under test: {@link OrderManagerApplication#logFilter()}
     */
    @Test
    void testLogFilter() {
        // Arrange, Act and Assert
        assertTrue((new OrderManagerApplication()).logFilter().getEnvironment() instanceof StandardServletEnvironment);
    }
}
