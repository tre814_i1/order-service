package fr.epsi.rennes.mspr.tpre814.orders.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.epsi.rennes.mspr.tpre814.shared.events.ProductRequest;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockRequest;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockResponse;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import fr.epsi.rennes.mspr.tpre814.shared.models.ProductDetails;
import org.apache.kafka.clients.admin.NewTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static fr.epsi.rennes.mspr.tpre814.config.KafkaConfig.RESERVE_TOPIC;
import static fr.epsi.rennes.mspr.tpre814.config.KafkaConfig.STOCK_RESPONSE;

/**
 * The main goal of this class is to wait for messages from the given topics to be consumed,
 * in order to execute a process and finally respond with an ACK.
 */
@Component
public class FakeStockEventListener { // auto plugs on tests with containers
    private static final Random rng = new Random();
    private static final Logger log = LoggerFactory.getLogger(FakeStockEventListener.class);
    private static final ObjectMapper mapper = new ObjectMapper();
    private final KafkaTemplate<UUID, String> kafkaTemplate;
    private final List<StockResponse> stockResponses = new ArrayList<>();

    FakeStockEventListener(KafkaTemplate<UUID, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Bean
    public NewTopic productTopic() {
        return TopicBuilder.name(RESERVE_TOPIC).partitions(2).replicas(1).build();
    }

    @KafkaListener(topics = RESERVE_TOPIC, groupId = "fake-products-service-1",
            autoStartup = "true", info = "Fake Products Service Listener")
    public void emulateReservation(String req) throws JsonProcessingException {
        log.info("Received new Products request: " + req);
        // Get the requested products
        StockRequest stockRequest = mapper.readValue(req.replace("products", "payload"), StockRequest.class);
        List<ProductRequest> products = stockRequest.getPayload();
        if (products.isEmpty()) {
            log.error("No products requested");
            return;
        }
        // Consider every product exists, with a random stock
        // Emulate the validation of the stockRequest
        List<Product> myProducts = new ArrayList<>(products.size());
        products.forEach(product -> {
            Product myProduct = new Product();
            myProduct.setId(product.id());
            myProduct.setQuantity(product.quantity());
            myProduct.setDetails(new ProductDetails());
            myProduct.getDetails().setPrice(rng.nextDouble() * 100);
            myProduct.setName("Product " + product.id());
            myProduct.setStock(rng.nextInt(100));
            myProducts.add(myProduct);
        });
        // response to the order-service
        StockResponse stockResponse = new StockResponse(stockRequest.getId(), myProducts);
        stockResponses.add(stockResponse);  // spy

        kafkaTemplate.send(STOCK_RESPONSE, stockRequest.getId(), stockResponse.toJson());
    }

    public List<StockResponse> getStockResponses() {
        return stockResponses;
    }

}
