package fr.epsi.rennes.mspr.tpre814.orders.api;

import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderNotFoundException;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderStatusException;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderValidationError;
import org.apache.kafka.common.TopicPartition;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.listener.KafkaBackoffException;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ErrorHandlerDiffblueTest {
    /**
     * Method under test:
     * {@link ErrorHandler#handleOrderNotFoundException(OrderNotFoundException)}
     */
    @Test
    void testHandleOrderNotFoundException() {
        // Arrange
        ErrorHandler errorHandler = new ErrorHandler();

        // Act
        ResponseEntity<String> actualHandleOrderNotFoundExceptionResult = errorHandler
                .handleOrderNotFoundException(new OrderNotFoundException("foo"));

        // Assert
        assertNull(actualHandleOrderNotFoundExceptionResult.getBody());
        assertEquals(404, actualHandleOrderNotFoundExceptionResult.getStatusCodeValue());
        assertTrue(actualHandleOrderNotFoundExceptionResult.getHeaders().isEmpty());
    }

    /**
     * Method under test:
     * {@link ErrorHandler#handleOrderValidationError(OrderValidationError)}
     */
    @Test
    void testHandleOrderValidationError() {
        // Arrange
        ErrorHandler errorHandler = new ErrorHandler();
        Order order = new Order();

        // Act
        ResponseEntity<String> actualHandleOrderValidationErrorResult = errorHandler
                .handleOrderValidationError(new OrderValidationError(order, new Throwable()));

        // Assert
        assertEquals("Order validation error for order null : null", actualHandleOrderValidationErrorResult.getBody());
        assertEquals(400, actualHandleOrderValidationErrorResult.getStatusCodeValue());
        assertTrue(actualHandleOrderValidationErrorResult.getHeaders().isEmpty());
    }

    /**
     * Method under test:
     * {@link ErrorHandler#handleEnumConstantNotPresentException(EnumConstantNotPresentException)}
     */
    @Test
    void testHandleEnumConstantNotPresentException() {
        // Arrange
        ErrorHandler errorHandler = new ErrorHandler();
        Class<Enum> forNameResult = Enum.class;

        // Act
        ResponseEntity<String> actualHandleEnumConstantNotPresentExceptionResult = errorHandler
                .handleEnumConstantNotPresentException(new EnumConstantNotPresentException(forNameResult, "foo"));

        // Assert
        assertEquals("Invalid state : foo", actualHandleEnumConstantNotPresentExceptionResult.getBody());
        assertEquals(400, actualHandleEnumConstantNotPresentExceptionResult.getStatusCodeValue());
        assertTrue(actualHandleEnumConstantNotPresentExceptionResult.getHeaders().isEmpty());
    }

    /**
     * Method under test:
     * {@link ErrorHandler#handleNullPointerException(NullPointerException)}
     */
    @Test
    void testHandleNullPointerException() {
        // Arrange
        ErrorHandler errorHandler = new ErrorHandler();

        // Act
        ResponseEntity<String> actualHandleNullPointerExceptionResult = errorHandler
                .handleNullPointerException(new NullPointerException("foo"));

        // Assert
        assertEquals("Null pointer exception", actualHandleNullPointerExceptionResult.getBody());
        assertEquals(400, actualHandleNullPointerExceptionResult.getStatusCodeValue());
        assertTrue(actualHandleNullPointerExceptionResult.getHeaders().isEmpty());
    }

    /**
     * Method under test:
     * {@link ErrorHandler#handleOrderStatusException(OrderStatusException)}
     */
    @Test
    void testHandleOrderStatusException() {
        // Arrange
        ErrorHandler errorHandler = new ErrorHandler();

        // Act
        ResponseEntity<String> actualHandleOrderStatusExceptionResult = errorHandler
                .handleOrderStatusException(new OrderStatusException("foo"));

        // Assert
        assertEquals("Order status is not valid : foo", actualHandleOrderStatusExceptionResult.getBody());
        assertEquals(422, actualHandleOrderStatusExceptionResult.getStatusCodeValue());
        assertTrue(actualHandleOrderStatusExceptionResult.getHeaders().isEmpty());
    }

    /**
     * Method under test: {@link ErrorHandler#handleException(Exception)}
     */
    @Test
    void testHandleException() {
        // Arrange
        ErrorHandler errorHandler = new ErrorHandler();

        // Act
        ResponseEntity<String> actualHandleExceptionResult = errorHandler.handleException(new Exception("foo"));

        // Assert
        assertEquals("Internal server error", actualHandleExceptionResult.getBody());
        assertEquals(500, actualHandleExceptionResult.getStatusCodeValue());
        assertTrue(actualHandleExceptionResult.getHeaders().isEmpty());
    }

    /**
     * Method under test:
     * {@link ErrorHandler#handleKafkaBackoffException(KafkaBackoffException)}
     */
    @Test
    void testHandleKafkaBackoffException() {
        // Arrange
        ErrorHandler errorHandler = new ErrorHandler();

        // Act
        ResponseEntity<String> actualHandleKafkaBackoffExceptionResult = errorHandler.handleKafkaBackoffException(
                new KafkaBackoffException("An error occurred", new TopicPartition("Topic", 1), "42", 1L));

        // Assert
        assertEquals("Service Unavailable An error occurred", actualHandleKafkaBackoffExceptionResult.getBody());
        assertEquals(503, actualHandleKafkaBackoffExceptionResult.getStatusCodeValue());
        assertTrue(actualHandleKafkaBackoffExceptionResult.getHeaders().isEmpty());
    }
}
