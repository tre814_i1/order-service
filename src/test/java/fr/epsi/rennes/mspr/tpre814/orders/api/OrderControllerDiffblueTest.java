package fr.epsi.rennes.mspr.tpre814.orders.api;

import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.orders.services.interfaces.OrderService;
import fr.epsi.rennes.mspr.tpre814.orders.utils.OrderMapper;
import fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus;
import fr.epsi.rennes.mspr.tpre814.shared.models.OrderDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.aot.DisabledInAotMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {OrderController.class})
@ExtendWith(SpringExtension.class)
@DisabledInAotMode
class OrderControllerDiffblueTest {
    @Autowired
    private OrderController orderController;

    @MockBean
    private OrderService orderService;

    /**
     * Method under test: {@link OrderController#create(Order)}
     */
    @Test
    void testCreate() {
        // Arrange
        OrderService orderService = mock(OrderService.class);
        OrderController orderController = new OrderController(orderService);

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        when(orderService.create(Mockito.any())).thenReturn(OrderMapper.toDTO(order));

        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());

        // Act
        OrderDTO actualCreateResult = orderController.create(order);

        // Assert
        verify(orderService).create(isA(Order.class));
        assertThat(actualCreateResult)
                .usingRecursiveComparison()
                .ignoringFields("createdAt", "updatedAt")
                .isEqualTo(OrderMapper.toDTO(order));

        verify(orderService, times(1)).create(Mockito.any());
        verify(orderService, times(1)).create(order);
        verifyNoMoreInteractions(orderService);
    }

    /**
     * Method under test: {@link OrderController#all()}
     */
    @Test
    void testAll() throws Exception {
        // Arrange
        when(orderService.all()).thenReturn(new ArrayList<>());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v2/order/all");

        // Act and Assert
        MockMvcBuilders.standaloneSetup(orderController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    /**
     * Method under test: {@link OrderController#update(Order)}
     */
    @Test
    void testUpdate() {
        //   Diffblue Cover was unable to create a Spring-specific test for this Spring method.

        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);

        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        OrderService orderService = mock(OrderService.class);
        when(orderService.update(Mockito.any())).thenReturn(OrderMapper.toDTO(order));
        OrderController orderController = new OrderController(orderService);

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());

        // Act
        OrderDTO actualUpdateResult = orderController.update(entity);

        // Assert
        verify(orderService).update(isA(Order.class));
        assertThat(actualUpdateResult)
                .usingRecursiveComparison()
                .isEqualTo(OrderMapper.toDTO(order));
    }

    /**
     * Method under test: {@link OrderController#delete(UUID)}
     */
    @Test
    void testDelete() throws Exception {
        // Arrange
        doNothing().when(orderService).delete(Mockito.any());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/v2/order/{orderId}",
                UUID.randomUUID());

        // Act and Assert
        MockMvcBuilders.standaloneSetup(orderController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Method under test: {@link OrderController#get(UUID)}
     */
    @Test
    void testGet() {
        //   Diffblue Cover was unable to create a Spring-specific test for this Spring method.

        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);

        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        OrderService orderService = mock(OrderService.class);
        when(orderService.get(Mockito.any())).thenReturn(OrderMapper.toDTO(order));
        OrderController orderController = new OrderController(orderService);

        // Act
        OrderDTO actualGetResult = orderController.get(UUID.randomUUID());

        // Assert
        verify(orderService).get(isA(UUID.class));
        assertThat(actualGetResult)
                .usingRecursiveComparison()
                .isEqualTo(OrderMapper.toDTO(order));
    }

    /**
     * Method under test: {@link OrderController#getCustomerOrders(UUID)}
     */
    @Test
    void testGetCustomerOrders() {
        // Arrange
        OrderService orderService = mock(OrderService.class);
        ArrayList<OrderDTO> orderDTOList = new ArrayList<>();
        when(orderService.getCustomerOrders(Mockito.<UUID>any())).thenReturn(orderDTOList);
        OrderController orderController = new OrderController(orderService);

        // Act
        List<OrderDTO> actualCustomerOrders = orderController.getCustomerOrders(UUID.randomUUID());

        // Assert
        verify(orderService).getCustomerOrders(isA(UUID.class));
        assertTrue(actualCustomerOrders.isEmpty());
        assertSame(orderDTOList, actualCustomerOrders);
    }
}
