package fr.epsi.rennes.mspr.tpre814.orders.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.orders.database.repository.OrderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class OrderListenerDiffblueTest {
    private OrderRepository repository;
    private OrderServiceImpl orderService;
    private KafkaTemplate<UUID, String> kafkaTemplate;
    private ProducerFactory<UUID, String> producerFactory;
    private OrderListener orderListener;
    @BeforeEach
    void setUp() {
        repository = mock(OrderRepository.class);
         orderService = mock(OrderServiceImpl.class);
        producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        kafkaTemplate = mock(KafkaTemplate.class);
        when(kafkaTemplate.send(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(null);
        orderListener = new OrderListener(orderService, kafkaTemplate);

    }
    /**
     * Method under test: {@link OrderListener#listenOrderRequests(String, UUID)}
     */
    @Test
    void testListenOrder() throws JsonProcessingException {
        // Arrange
        Order order = new Order();
        order.setId(UUID.randomUUID());

        List<UUID> requested = List.of(order.getId());
        when(repository.findById(order.getId())).thenReturn(Optional.of(order));


        // Act
        orderListener.listenOrderRequests(requested.toString(), UUID.randomUUID());

        // Assert
        verify(repository, times(0)).findById(order.getId());
        verify(kafkaTemplate, times(1)).send(Mockito.any(), Mockito.any(), Mockito.any());
    }

    /**
     * Method under test: {@link OrderListener#listenOrderRequests(String, UUID)}
     */
    @Test
    void testListenOrder2() throws JsonProcessingException {
        // Arrange
        ObjectMapper objectMapper = new ObjectMapper();
        String req = objectMapper.writeValueAsString(new ArrayList<>());

        // Act
        assertThrows(IllegalArgumentException.class, () -> orderListener.listenOrderRequests(req, UUID.randomUUID()));
    }

    /**
     * Method under test: {@link OrderListener#listenOrderRequests(String, UUID)}
     */
    @Test
    void testListenOrder_ok() throws JsonProcessingException {
        // Arrange
        List<UUID> requested = List.of(UUID.randomUUID());
        when(repository.findById(Mockito.any())).thenReturn(Optional.of(new Order()));
        // Act
        orderListener.listenOrderRequests(requested.toString(), UUID.randomUUID());

        // Assert
        verify(orderService, times(1)).getAllRequested(Mockito.any());
        verify(kafkaTemplate, times(1)).send(Mockito.any(), Mockito.any(), Mockito.any());
    }
}
