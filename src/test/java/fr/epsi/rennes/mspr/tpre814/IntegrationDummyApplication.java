package fr.epsi.rennes.mspr.tpre814;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.scheduling.annotation.EnableAsync;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.sql.Connection;
import java.sql.SQLException;


@Configuration
@Testcontainers
@EnableKafka
@EnableJpaRepositories
@EnableAsync
@EnableAspectJAutoProxy
@AutoConfigureDataJpa
@SpringBootApplication
public class IntegrationDummyApplication {
    // DOCKER CONTAINERS
    @Container
    public static final PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer(
            DockerImageName.parse("postgres:16-alpine")
                    .asCompatibleSubstituteFor("postgres")
    )
            .withDatabaseName("postgres")
            .withUsername("sa")
            .withPassword("sa");
    // KAFKA CONTAINERS
    @Container
    public static final KafkaContainer kafka = new KafkaContainer(
            DockerImageName.parse("confluentinc/cp-kafka:7.6.1")
                    .asCompatibleSubstituteFor("confluentinc/cp-kafka")
    );

    public static void main(String[] args) {
        System.out.println("""
                #########################################################################################
                #########################################################################################

                IntegrationDummyApplication.init

                #########################################################################################
                #########################################################################################
                """);
        SpringApplication.run(IntegrationDummyApplication.class, args);
    }

    public static String getKafkaBootstrap() {
        return kafka.getBootstrapServers();
    }

    public static Connection getPostgresConnection() throws SQLException {
        return postgreSQLContainer.createConnection("");
    }

    public static void start() {
        postgreSQLContainer.start();
        kafka.start();
    }

    public static String getPostgresUrl() {
        return "jdbc:tc:postgresql:16-alpine://localhost:" + postgreSQLContainer.getMappedPort(5432) + "/postgres";
    }

    public static void stop() {
        postgreSQLContainer.stop();
        kafka.stop();
    }

    public static String getPostgresUsername() {
        return postgreSQLContainer.getUsername();
    }

    public static String getPostgresPassword() {
        return postgreSQLContainer.getPassword();
    }
}
