#!/usr/bin/env sh
# This script is used to build Image and push to docker hub

gradle clean bootBuildImage
# shellcheck disable=SC2181
if [ $? -eq 0 ]; then
  echo "Build successful"
  # requires docker login on current machine
  docker push mistayan/order-service:v0.6.6
  docker push mistayan/order-service:latest
else
  echo "Build failed"
fi
# wait user input to exit
read -p "Press enter to exit"
